<?php

namespace App;

use App\UserInstagrams;
use App\ChargebeeSubscription;
use App\Notifications\PasswordReset;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use TijmenWierenga\LaravelChargebee\Billable;
use TijmenWierenga\LaravelChargebee\HandlesWebhooks;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class User extends Authenticatable
{
    use Notifiable, Billable, HandlesWebhooks, HasRolesAndAbilities;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $statusLists = [
        'active' => 1,
        'pending' => 2,
        'suspended' => 3,
        'deactivated' => 4,
    ];


    public function setStatusAttribute( $value )
    {
        $value = isset($this->statusLists[$value])
            ? $this->statusLists[$value]
            : 1 ;

        $this->attributes['status'] = $value;
    }

    public function userInstagrams(  )
    {
        return $this->hasMany(UserInstagrams::class);
    }

    public function chargebeeSubscription(  )
    {
        return $this->hasOne(ChargebeeSubscription::class);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }
}
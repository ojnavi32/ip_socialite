<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserInstagrams extends Model
{
    protected $fillable = [
    	'user_id', 'name', 'hashtags', 'campaign'
    ];

    public function user(  )
    {
    	return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;

class GuestController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function create(Request $request)
    {
        return $request;
        if (User::where('email', '=', $request['email'])->count() > 0) {
            return '/login';
        }else{
            $user = new User;

            $user->name = $request['name'];
            $user->instagramname = $request['instagramname'];
            $user->email = $request['email'];
            $user->hashtags = $request['hashtags'];
            $user->reference = $request['password'];
            $user->password = bcrypt($request['password']);
            $user->plan = $request['plan'];
    
            $user->save();
            Auth::login($user);
            
            return redirect('/setup');
    
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use DB;
use App\User;
use Carbon\Carbon;
use App\Events\NotifyAdmin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        $response =  DB::table('password_resets')
            ->where('token', $request['token'])
            ->get();

        if (count($response) > 0) {
            $user = User::where('email', $request->email)->first();
            $user->password = bcrypt($request['password']);

            if ($user->status === 'pending') {
                $user->status = 'active';
                $this->sendNotificationToAdmin($user, 'New User');
            } else {
                $this->sendNotificationToAdmin($user, 'Update Password');
            }

            $user->save();

            $this->deleteToken($request['email']);

            return redirect($this->redirectPath());
        }

        return $this->sendResetFailedResponse($request, $response);
    }

    public function deleteToken( $email )
    {
        \DB::table('password_resets')
           ->where('email', $email)
           ->delete();
    }

    public function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|exists:users,email',
            'password' => ['required',
                        'min:7',
                        'confirmed',
            ],
        ];
    }

    public function sendNotificationToAdmin( $user, $action )
    {
        $data = [
            'user' => $user,
            'action' => $action
        ];
        if (!Bouncer::is($user)->an('admin')) {
            event(new NotifyAdmin($data));
        }
    }
}

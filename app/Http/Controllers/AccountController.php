<?php
namespace App\Http\Controllers;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('/myaccount');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function startUpdate(Request $request, $id)
    {
        $user = User::find($id);

        $user->name = $request->name;
        $user->instagramname = $request->instagramname;
        $user->hashtags = $request->hashtags;
        
        $user->save();

        return redirect('/thankyou');
                //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if($request->get('instampassword')!=$request->get('instampasswordconfirm')){
            return redirect()->back()->with("error","Instagram Password and Confirmation Password didn't matched. Please make sure they match.");
        }

        $user->instagramname = $request->instagramname;
        $user->phone = $request->phone;
        $user->instampassword = $request->instampassword;
        $user->campaign = $request->campaign;
        
        $user->save();

        return redirect('/thankyou');
    }

    public function thankyou(){
        return view('thankyou');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request)
    {

        $user = User::find(Auth::user()->id);

        $user->name = $request->name;
        $user->instagramname = $request->instagramname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->instampassword = $request->instampassword;
        $user->plan = $request->plan;
        $user->phone = $request->phone;
        $user->hashtags = $request->hashtags;
        $user->campaign = $request->campaign;
        
        $user->save();

        return redirect()->back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendEmail()
    {
        return redirect('/emailsent');
    }

    public function showChangePasswordForm(){
        return view('auth.changepassword');
    }

    public function changePassword(Request $request){
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
        // The passwords matches
        return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
        //Current password and new password are same
        return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        if($request->get('new-password')!=$request->get('new-password_confirmation')){
            return redirect()->back()->with("error","New Password and Confirm New Password didn't matched. Please make sure they match.");
        }

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success","Password changed successfully !");
    }


    public function showChangeInstaPasswordForm(){
        return view('auth.changeInstaPassword');
    }

    public function changeInstaPassword(Request $request){
        if ($request->get('current-password')!=Auth::user()->instampassword) {
        // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
        //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        if($request->get('new-password')!=$request->get('new-password_confirmation')){
            return redirect()->back()->with("error","New Password and Confirm New Password didn't matched. Please make sure they match.");
        }

        //Change Password
        $user = Auth::user();
        $user->instampassword = $request->get('new-password');
        $user->save();
        return redirect()->back()->with("success","Instagram Password changed successfully!");
    }
    
}

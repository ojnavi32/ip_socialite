<?php

namespace App\Http\Controllers;

use DB;
use Bouncer;
use App\User;
use Carbon\Carbon;
use App\Events\NotifyAdmin;
use App\UserInstagrams;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id', '<>', Auth::user()->id)
            ->with('roles')->paginate(10);

        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user['name'] = $request['name'];
        $user['email'] = $request['email'];
        $user['phone'] = $request['phone'];
        $user->save();

        if (!Bouncer::is($user)->an('admin')) {
            event(new NotifyAdmin($user['name'], 'Updated User Info'));
        }

        return redirect('/app/user/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function profile(  )
    {
        $user = Auth::user();

        return view('user.profile', compact('user'));
    }

    public function startnowRegister( Request $request )
    {
        $user = User::create([
            'name' => $request['firstname'].' '.$request['lastname'],
            'email' => $request['email'],
            'password' => bcrypt(rand(111,999)),
        ]);

        $token = str_random(60);
        DB::table('password_resets')->insert([
            'email' => $request['email'],
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        $user->sendPasswordResetNotification($token);
    }
}

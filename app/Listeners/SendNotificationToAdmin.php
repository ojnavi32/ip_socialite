<?php

namespace App\Listeners;

use Bouncer;
use App\User;
use App\Events\NotifyAdmin;
use App\Notifications\NotifyAdmin as NotificationNotifyAdmin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNotificationToAdmin
{
    /**
     * Handle the event.
     *
     * @param  NotifyAdmin  $event
     * @return void
     */
    public function handle(NotifyAdmin $event)
    {
        $admin = User::whereIs('admin')->first();
        $admin->notify(new NotificationNotifyAdmin($event->user, $event->action));
    }
}

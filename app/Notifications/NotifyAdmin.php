<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotifyAdmin extends Notification implements ShouldQueue
{
    use Queueable;

    public $user, $action;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $action)
    {
        $this->user = $user;
        $this->action = $action;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Socialite.Online New Notification!')
            ->line('User:'.$this->user)
            ->line('Action:'.$this->action);
    }

    public function toDatabase( $notifiable )
    {
        return [
            'user' => $this->user,
            'action' => $this->action
        ];
    }

    public function toBroadcast( $notifiable )
    {
        return [
            'user' => $this->user,
            'action' => $this->action
        ];
    }
}

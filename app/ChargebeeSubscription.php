<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ChargebeeSubscription extends Model
{
    protected $fillable = [
    	'user_id', 'chargebee_id', 'plan_id'
    ];

    public function user(  )
    {
    	return $this->belongsTo(User::class);
    }
}

<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('welcome');
});
Route::get('/howitworks', function () {
    return view('howitworks');
});
Route::get('/pricing', function () {
    return view('pricing');
});
Route::get('/reviews', function () {
    return view('reviews');
});
Route::get('/contact', function () {
    return view('contact');
});

Route::get('/instagramlikes', function () {
    return view('instagramlikes');
});
Route::get('/instagramfollowers', function () {
    return view('instagramfollowers');
});
Route::get('/privacypolicy', function () {
    return view('privacypolicy');
});
Route::get('/disclaimer', function () {
    return view('disclaimer');
});
Route::get('/returnpolicy', function () {
    return view('returnpolicy');
});
Route::get('/termsofuse', function () {
    return view('termsofuse');
});

Route::get('/thankyou', [
    'uses'=>'AccountController@thankyou'
]);

Route::get('/emailsent', function () {
    return view('emailsent');
});

Route::get('/startnow', function () {
    return view('startnow');
});


Route::post('/startnowRegister', 'UsersController@startnowRegister');

// Route::post('/startaccountupdate/{id}', [
//     'uses'=> 'AccountController@startUpdate'
// ]);

Route::post('/updateaccount/{id}', [
    'uses'=> 'AccountController@update'
]);

Route::post('/updateProfile', [
    'uses'=> 'AccountController@updateUser'
]);

Route::get('/myaccount', [
    'uses'=> 'AccountController@index'
]);

Route::get('/setup', function () {
    return view('setup');
});

Route::get('/setupInstaFollower', function () {
    return view('setupInstaFollower');
});

Route::get('/changePasswordPage','AccountController@showChangePasswordForm');

Route::post('/changePassword','AccountController@changePassword')->name('changePassword');

Route::get('/changeInstagramPasswordPage','AccountController@showChangeInstaPasswordForm');

Route::post('/changeInstaPassword','AccountController@changeInstaPassword')->name('changeInstaPassword');
 
// Route::get('/welcome', 'HomeController@index')->name('welcome');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

use App\Events\NotifyAdmin;

Route::get('/tesss', function () {
    $data = [
        'user' => 'Socialite Customer',
        'action' => 'Reset Password'
    ];
    event(new NotifyAdmin('Socialite Customer', 'Reset Password'));
    return ['success'];
});


Route::group(['prefix' => 'app', 'middleware' => 'auth'], function () {
    Route::get('/', function () {
        return \Auth::user()->unreadNotifications()->get();
    });
    #User routes
    Route::get('/user/profile', 'UsersController@profile');
    Route::resource('/user', 'UsersController');

    #Notifications
    Route::resource('/notifications', 'NotificationsController');
});

#Custom/Override Routes
Route::get('/userLogout', 'Auth\LoginController@logout');
<?php

use Bouncer as Bounce;
use App\User;
use Illuminate\Database\Seeder;

class UserWithProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminAbilities = [
        	'manage-users',
        	'view-notifications'
        ];

        $customerAbilities = [
        	'manage-subscription'
        ];

        Bounce::role()->create([
        	'name' => 'admin',
        	'title' => 'Admin'
        ]);

        Bounce::role()->create([
        	'name' => 'customer',
        	'title' => 'Customer'
        ]);

        $admin = User::create([
        	'name' => 'Socialite Admin',
        	'email' => 'admin@socialite.com',
        	'password' => bcrypt('password'),
        	'phone' => '+6300',
        	'status' => 1
        ]);

        Bounce::assign('admin')->to($admin);
        Bounce::allow($admin)->to($adminAbilities);

        $customer = User::create([
        	'name' => 'Socialite customer',
        	'email' => 'customer@socialite.com',
        	'password' => bcrypt('password'),
        	'phone' => '+6200',
        	'status' => 1
        ]);

        Bounce::assign('customer')->to($customer);
        Bounce::allow($customer)->to($customerAbilities);

    }
}

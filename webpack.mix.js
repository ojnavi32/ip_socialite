let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .styles([
   		'resources/assets/theme/css/dashboard.min.css',
   		'resources/assets/theme/css/vendor.css',
   		'node_modules/toastr/build/toastr.min.css'
   	], 'public/css/all.css')
   .js([
   		'resources/assets/theme/js/dashboard.js',
   		'node_modules/toastr/build/toastr.min.js'
   	], 'public/js/dashboard.js');


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="7kreQuLpxbORTzAMuXsKsEh9sxQ65dSpjn8Gjat1">
    <link rel="shortcut icon" type="image/x-icon" href="http://socialitedev.kohlkreative.com/favicon.ico"/>
    <title>Socialite</title>
    <link rel="stylesheet" href="https://use.typekit.net/gvl6bsk.css">
    <link rel="stylesheet" href="https://use.typekit.net/irw3hsg.css">
    <link rel="stylesheet" href="https://use.typekit.net/gbm0xte.css">
    <!-- Styles -->
    <link rel='stylesheet prefetch' href='/css/font-awesome.min.css'>
    <link href="http://socialitedev.kohlkreative.com/css/app.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="/js/jquery.min.js"></script>    
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script src="//code.tidio.co/hvutxhnh982rb2rxkllyc1vlbwqjybaz.js"></script>
    <script src="http://socialitedev.kohlkreative.com/js/app.js"></script>
   <style>
        .container{
            margin:0px;
            width:100%;
            max-width:96%;
            position:relative;
        }
        .navbar-header{
            width:30%;
            max-width:390px;
            position:absolute;
            left:0px;
            top:-10px;
        }
        #app-navbar-collapse{
            position:absolute;
            right:0px;
            width:70%;
            top:-8px;
        }
        #menuFS{
            float:right;
        }
        .carousel-caption{
            top:45%;
            height:200px;
        }
        .backover{
            background-color:rgba(0, 0, 0, 0.1);
            width:80%;
            padding-top:30px;
            padding-bottom:30px;
            margin:auto;
        }
        .carousel-caption {
            top: 40%;
            bottom: auto;
        }
        .carousel-caption h3, .carousel-caption p{
            color:white;
        }
        .backover h3{
            font-size:18px;
        }
        .backover p{
            font-size:14px;
        }
        .twtextl h2{
            font-family:'futura-pt';
            font-weight:300;
            font-size:42px;
        }
        .twtextl h3{
            font-family:'futura-pt';
            font-weight:500;
            font-size:90px;
            color:white;
        }
        .twtextl .blackh3{
            font-family:'futura-pt';
            font-weight:100;
            font-size:42px;
            color:black;
            text-align:center;
        }
        .twtextl .whitep{
            font-family:'futura-pt';
            font-weight:300;
            font-size:21px;
            color:white;
        }

        .twtextl p{
            font-family:'futura-pt';
            font-size:100;
            font-size:18px;
        }
        .brkline{
            height:40px;
            width:70%;
            margin-left:15%;
            clear:both;
            border-top:2px solid #4A4A4A;
        }
        .brk{
            height:40px;
            width:70%;
            margin-left:15%;
            clear:both;
        }
        .boxed ul{
            margin-left:22px;
            text-align:left;
        }
        .boxed{
            font-family:'freight-sans-pro';
            border:2px solid #4A4A4A;
            color:#4A4A4A;
            height:850px;
            margin:50px 10px;
            padding:50px 10px;
        }
        .boxed h2{
            font-size:48px;
            font-weight:700;
            text-align:center;
        }
        .boxed h3{
            font-size:32px;
            font-weight:700;
            text-align:center;
        }
        .boxed h4, .boxed del{
            font-size:24px;
            font-weight:700;
            text-align:center;
            margin:20px 0px;
        }
        .boxed h5{
            font-size:21px;
            font-weight:300;
            text-align:center;
        }
        .boxed li{
            font-size:20px;
            font-weight:300;
        }
        .contentsDiv h2{
            font-size:42px;

        }
        .twtextlp img{
            width:378px;
            height:auto;
        }
        .fptmp{
            font-size:18px;
        }
        .fptmh2{
            font-size:52px;
        }
        .fptmh3{
            font-size:38px;
        }
        .fptmh4{
            font-size:24px;
        }
        .fptmh5{
            font-size:18px;
        }
        .row.no-pad {
            margin-right:0;
            margin-left:0;
        }
        .row.no-pad > [class*='col-'] {
            padding-right:0;
            padding-left:0;
        }
        .btnb{
            border: 0;
            border-radius: 0;
            background: #4C4C4C;
            -webkit-appearance: none;
            color:white;
            padding:15px 30px;
            letter-spacing:4px;
            font-size:16px;
        }
        .accordion{
            margin-left:50px;
        }
 /* accordion styles */
.accordion .card-header {
  cursor: pointer;
}
.accordion.heading-right .card-title {
  position: absolute;
  right: 50px;
}
.accordion.indicator-plus .card-header:after {
  font-family: 'FontAwesome';
  content: "\f068";
  float: right;
}
.accordion.indicator-plus .card-header.collapsed:after {
  content: "\f067";
}
.accordion.indicator-plus-before.round-indicator .card-header:before {
  font-family: 'FontAwesome';
  font-size: 14pt;
  content: "\f056";
  margin-right: 10px;
  margin-top:10px;
}
.accordion.indicator-plus-before.round-indicator .card-header.collapsed:before {
  content: "\f055";
  color: #000;
}
.accordion.indicator-plus-before .card-header:before {
  font-family: 'FontAwesome';
  content: "\f068";
  float: left;
}
.accordion.indicator-plus-before .card-header.collapsed:before {
  content: "\f067";
}
.accordion.indicator-chevron .card-header:after {
  font-family: 'FontAwesome';
  content: "\f078";
  float: right;
}
.accordion.indicator-chevron .card-header.collapsed:after {
  content: "\f077";
}
.accordion.background-none [class^="card"] {
  background: transparent;
}
.accordion.border-0 .card {
  border: 0;
}

.col-6 h2 em, .col-12 h2 em, .col-7 h2 em{
    font-size:32px;
    font-style:italic;
    font-weight:500;
}

#navbar {
  background-color: #151515;
  position: fixed;
  top: -121px;
  width: 100%;
  display:block;
  height:121px;
  transition: top 0.3s;
  left:0px;
  padding-top:20px;
}
.navbar-hidden{
    position:fixed;
    left:3%;
}
.menu-hidden{
    position:fixed;
    right:3%;
    font-family: "futura-pt";
	font-weight:700;
	font-size:18px;
	font-weight: 600;
    letter-spacing: 3px;
    list-style: none;
    width:70%;
    min-width:750px;
}
.menu-hidden li{
    float:right;
    margin:26px 15px;
}
.menu-hidden li a{
    text-decoration:none;
	color:white!important;
	transition: .2s;
	white-space: none;
}
.menu-hidden li a:hover{
	color:#9c9999!important;
	background:none;
	transition: .2s;
}

/* Dropdown Button */
.dropbtn {
    background:none;
    color: white;
    padding: 18px;
    font-size: 18px;
    letter-spacing:3px;
    border: none;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
    position: relative;
    display: inline-block;
    margin-top:-8px;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #262626;
    color:white;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    font-weight:700;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {
    background-color: #ddd;
    color:#9c9999;
}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
    display: block;
}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {
    color:#9c9999;
}
.navs1, .navs2div {
    display:none;
}
.twtextl.carousel{
    width:1020px;
    height:1020px;
}
.carousel-item img{
    width:1020px;
    height:auto;
}
.twtextl{
    width:100%;
}

#carousel0 .carousel-inner .carousel-item img{
    width:300px!important;
}
#carousel1{
    width:100%;
    max-width:1020px;
    margin:0 auto;
}
#carousel1 .carousel-inner .carousel-item img{
    width:100%;
    height:100%;
    max-width:1020px;
    max-height:1020px;
}
#desktab{
    display:block;
}
#phonetab{
    display:none;
}
.desktab{
    display:block;
}
.tabtab{
    display:none;
}.phonetab{
    display:none;
}
.twtextl .ax{
    padding:35px 0px;
}
#theprocess{
    margin-top:50px;
    font-weight:300;
    font-size:90px!important;
}
#whatwewill, #whatwewill em{
    margin-top:40px;
    font-weight:300;
    font-size:70px!important;
}
#whatwewill em{
    font-style:italic;
    font-weight:500;
}
#whatwewilldo{
    font-size:70px;
    margin-top:50px;
}
@media  only screen and (max-width: 1465px){
    .menu-hidden, #menuFS {
        display:none;
    }
    .navs1 {
        display:block;
    }
    .navs1 li{
        float:right;
        margin: 8px 5px;
        letter-spacing:1px;
        color:#fff;
        list-style:none;
    }
    .navs1 li a{
        font-size:16px;
        font-weight:500;
        color:#fff;
    }
    .dropbtn, .navs1>a{
        font-size:16px;
        font-weight:500;
        padding:0px;
        letter-spacing:1px;
    }
    .navs1{
        margin-right:-30px;
        margin-top:6px;
    }
}

@media (min-width: 620px) and (max-width: 1024px) {
    body{
        width:100vw;
        max-width:1024px;
    }
    .navbar-left,.navbar-right {
        float: none !important;
    }
    .navbar-toggle {
        display: block;
        position:fixed;
        right:20px;
    }
    #app-navbar-collapse{
        width:170px;
        margin-right:70px;
        margin-top:2px;
        overflow:hidden;
        border-top: 0px solid gray;
        border-color:transparent;
        background:rgba(50,50,50,.75);
        position:absolute;
    }
    #navbar{
        display:none;
    }
    .navbar-fixed-top {
        top: 0;
        border-width: 0 0 1px;
    }
    .navbar-collapse.collapse {
        display: none!important;
    }
    .navbar-nav {
        float: none!important;
        margin-top: 7.5px;
    }
    .navbar-nav>li {
        float: none;
    }
    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .collapse.in{
        display:block !important;
    }
    .navs1 li{
        clear:both;
    }
    .navs1 ul{
        margin-left:5px!important;
    }
    .navs1{
        margin-right:0px;
        margin-top:-30px;
    }
    .navbar-collapse{
        padding:0px;
    }
    .navbar-toggle{
        color:white;
        font-weight:700;
    }
    .navbar-default .navbar-toggle{
        width:50px;
        background:none;
    }
    .navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover {
        background:none;        
    }
    .dropdown-content{
        right:100px;
        top:25px;
    }
    #desktab{
        display:none;
    }
    #phonetab{
        display:block;
    }
    .texttitle .col-lg-4 {
        margin: 20px 0px;
    }

    #twtextlf{
        height:500px!important;
        width:100%;
    }
    .texttitle{
        width:100%!important;
    }
    .twtextlp img{
        width:100vw;
        height:auto;
    }
    .desktab{
        display:none;
    }
    .tabtab{
        display:block;
    }.phonetab{
        display:none;
    }
    .texttitle .col-lg-4 {
        margin: 30px auto;
    }
}
@media (min-width: 320px) and (max-width: 620px) {
    
    /* For mobile phones: */
    [class*="col-"] {
        width: 92%!important;
        max-width:100vw;
    }
    body{
        width:100%!important;
        max-width:100vw;
    }
    #app{
        width:100%!important;
        max-width:100vw;
    }
    .navbar-header{
        width:100%;
        position:absolute;
        max-width:510px;
        right:5px;
    }
    #app-navbar-collapse{
        margin-right:5px;
        overflow:hidden;
        float:right;
        margin-top:55px;
        background:rgba(50,50,50,.75);
    }
    .navs1{
        display:none
    }
    .navbar-header button{
        position:absolute;
        right:-15px;
    }
    .navbar-brand img{
        margin-left:-10px;
        width:220px;
    }
    .navbar-hidden img{
        margin-left:10px;
        width:220px;
    }
    #navbar{
        height:90px;
    }
    #app-navbar-collapse .navs1{
        display:block;
        text-align:right;
        margin-right:-10px;
    }
    .navbar-nav{
        display:none;
    }
    .dropdown-content{
        right:5px;
    }
    .navbar-toggle{
        right:10px;
    }
    .navbar-collapse{
        width:100%;
        max-width:190px;
    }
    .navbar-default .navbar-toggle{
        width:40px;
        background:none;
        color:white;
    }
    .navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover {
        background:none;        
    }
    .contentsDiv{
        width:94%;
        margin:0px auto;
    }
    .tbtext{
        margin-top:-40px;
    }
    .fpt70{
        font-size:50px;
    }
    .fpt36{
        font-size:25px;
    }
    .texttitle h3{
        margin-top:-30%;        
    }
    #mbchange{
        font-size:60px!important;
        margin-top:-30%;
        margin-right:10px;
    }
    .startb a{
        width:100%;
    }
    .texttitle{
        max-width:100vw;
    }
    .texttitle .col-lg-4{
        margin:20px auto;
    }
    .twtextl{
        height:500px!important;
    }
    .twtextl p{
        width:95%!important;
        margin:0px auto;
    }
    .backover h3, .backover p{
        color:black;
    }
    .twtextl{
        width:100%!important;
    }
    .footer{
        width:100%;
        height:650px;
    }
    .footcont{
        width:96%!important;
    }
    .sectionb{
        width:100vw;
        margin:0 auto;
    }
    .accordion{
        margin:0px;
    }
    .card-header a{
        display:inline-block;
        font-size:19px;
        line-height:1em;
        height:auto!important;
        margin:auto 10px;
    }
    .card-header{
        height:80px!important;
    }
    #carousel1{
        width:100%;
        max-width:1020px;
    }
    #carousel1 .carousel-inner .carousel-item img{
        width:96%;
        height:auto;
        max-width:1020px;
        max-height:1020px;
    }
    .carousel-inner{
        width:96%;
        height:auto;
    }
    .carousel-inner a{
        height:100%
    }
    .texttitle .col-lg-4 {
        margin: 20px 0px;
    }
    .footcont img{
        width:280px
    }
    #desktab{
        display:none;
    }
    #phonetab{
        display:block;
        margin-top:-150px;
    }
    .desktab{
        display:none;
    }
    .tabtab{
        display:none;
    }.phonetab{
        display:block;
    }
    .carousel-item img{
        margin:0px auto;
    }
    .twtextl h3{
        font-family:'futura-pt';
        font-weight:500;
        font-size:32px;
        color:white;
    }
    #theprocess{
        margin-top:50px;
        font-size:45px!important;
    }
    #whatwewill{
        margin-top:25px;
        font-size:35px!important;
    }
    #whatwewill em{
        font-size:35px!important;
        font-style:italic;
        font-weight:500;
    }
    /* body{
        background:black!important;
    } */
}
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <div id="navbar">
                    <a class="navbar-hidden" href="http://socialitedev.kohlkreative.com">
                        <img src="/images/logowhite.png" alt="Socialite">
                    </a>
                    <ul class="navs1" style="margin-right:10px;margin-top:22px;">
                                                    <li><div class="startm" style="width:130px;height:50px;padding-top:8px;margin-top:-10px;"><a href="/pricing">START NOW</a></div></li>
                                                <li class="dropdown">
                            <a href="/instagramlikes" class="dropbtn" >MORE SERVICES</a>
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="/instagramlikes">BUY LIKES</a>
                                <a class="dropdown-item" href="/instagramfollowers">BUY FOLLOWERS</a>
                            </div>
                        </li>                    
                        <li class="menuitems"><a href="/contact">SUPPORT</a></li>
                        <li class="menuitems"><a href="/reviews">REVIEWS</a></li>
                        <li class="menuitems"><a href="/pricing">PRICING</a></li>
                        <li class="menuitems"><a href="/howitworks">HOW IT WORKS</a></li>
                    </ul>

                    <ul class="menu-hidden">
                                                    <li style="margin:16px 2px 10px 1px;"><div class="startm"><a href="/pricing">START NOW</a></div></li>
                                                <li class="dropdown" style="margin-top:8px;margin-left:-10px;">
                            <button href="/instagramlikes" class="dropbtn" >MORE SERVICES</button>
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="/instagramlikes">BUY LIKES</a>
                                <a class="dropdown-item" href="/instagramfollowers">BUY FOLLOWERS</a>
                            </div>
                        </li>
                        <li><a href="/contact">SUPPORT</a></li>
                        <li><a href="/reviews">REVIEWS</a></li>
                        <li><a href="/pricing">PRICING</a></li>
                        <li><a href="/howitworks">HOW IT WORKS</a></li>
                    </ul>
                    </div>
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="fa fa-angle-double-down"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="http://socialitedev.kohlkreative.com">
                        <img src="/images/logowhite.png" alt="Socialite">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse" style="border-color:transparent;">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul id="menuFS" class="nav">
                        <!-- Authentication Links -->
                        <li><a href="/howitworks">HOW IT WORKS</a></li>
                        <li><a href="/pricing">PRICING</a></li>
                        <li><a href="/reviews">REVIEWS</a></li>
                        <li><a href="/contact">SUPPORT</a></li>
                        <li class="dropdown">
                            <button href="/instagramlikes" class="dropbtn" >MORE SERVICES</button>
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="/instagramlikes">BUY LIKES</a>
                                <a class="dropdown-item" href="/instagramfollowers">BUY FOLLOWERS</a>
                            </div>
                        </li>

                                                    <li><div class="startm"><a href="/pricing">START NOW</a></div></li>
                                            </ul>
                    <ul class="navs1" style="">
                                                    <li><div class="startm" style="width:130px;height:50px;padding-top:8px;margin-top:-10px;"><a href="/pricing">START NOW</a></div></li>
                                                <li class="dropdown">
                            <a href="/instagramlikes" class="dropbtn" >MORE SERVICES</a>
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="/instagramlikes">BUY LIKES</a>
                                <a class="dropdown-item" href="/instagramfollowers">BUY FOLLOWERS</a>
                            </div>
                        </li>                    
                        <li class="menuitems"><a href="/contact">SUPPORT</a></li>
                        <li class="menuitems"><a href="/reviews">REVIEWS</a></li>
                        <li class="menuitems"><a href="/pricing">PRICING</a></li>
                        <li class="menuitems"><a href="/howitworks">HOW IT WORKS</a></li>
                    </ul>
 
                </div>
            </div>
        </nav>

        <style>
    .form-group label{
        font-weight:300;
        font-size:18px;
    }
</style>

<div class="content">
    <div class="contentsDiv">
        <div class="tbtext" style="min-height:121px;">
        </div>
        <div class="topimage" style="height:121px;">
            <div class="color-overlay" style="background:#151515;position:relative;"></div>
        </div>
    </div>
    <div class="contentsDiv" style="font-family:'futura-pt';font-weight:300;">
        <h2 style="text-align:center;">Complete Your Account Setup</h2><br><br>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <form>
                            <div class="form-group"><label>Instagram Handle (Name) *</label><input class="form-control" type="text"></div>
                            <div class="form-group"><label>Phone # *</label><input class="form-control" type="text"></div>
                            <div class="form-group"><label>Email Address *</label><input class="form-control" type="email"></div>
                            <div class="form-group"><label style="width:100%;">Instagram Password *</label><label>Often times people will just change their password to something simple. "Your information is protected with SSL protection"</label><input class="form-control" type="password"></div>
                            <div
                                class="form-group"><label style="width:100%;">Confirm Your Instagram Password *</label><input class="form-control" type="password"></div>
                    <div class="form-group"><label style="width:100%;">4-5 generic that we can use in your campaign**</label><textarea class="form-control" rows="5"></textarea></div>
                    <div class="form-group" style="text-align:center;margin:40px 0px;"><button class="btnb" type="button">COMPLETE SETUP</button></div>
                    <div class="form-group">
                        <p>**These should be comments that will work with almost any Instagram picture. A lot of people will use Emoji's or comments like "That's awesome", "Keep up the good work", "I love your content" etc. You probably get the point now so
                            just put your own spin on it. Also if you need any help setting up your campaign you can email or call us.</p>
                    </div>
                    </form>
                </div>
                <div class="col" style="background-image:url(&quot;/images/download.jpg&quot;);background-size:cover;background-repeat:no-repeat;background-position:center;"></div>
            </div>
            </div>
        </section>
        <br><br> 
    </div>
</div>


        <div class="twtextl" id="twtextlf" style="height:243px;text-align:center;background:#F7F7F7;">
            <div class="texttitle" style="width:990px;margin:0 auto;z-index:150;padding-top:30px">

                <div class="col-lg-4 ">
                    <div class="startb"><a href="/howitworks">WHAT WE DO</a></div>
                </div>
                <div class="col-lg-4">
                    <div class="startr"><a href="/pricing">START NOW</a></div>
                </div>
                <div class="col-lg-4">
                    <div class="startb" style="width:234px;"><a href="/contact">CUSTOMER SERVICE</a></div>
                </div>
                <div class="col-lg-12" style="text-align:center;font-size:18px;margin-top:10px;font-family:'futura-pt';">
                    <br>
                    <p style="letter-spacing:3px;line-spacing:1.2em">MONEY BACK GUARANTEE<br></p>
                    <p style="font-family:'europa';margin-top:10px;">If your account doesn't see growth we will give you a refund!</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <br><br><br>
        <div class="footcont">
            <img src="/images/logowhite.png" alt="Socialite"><br><br><br>

            © 2017 The Socialite Technology LLC<br>

            Results may vary from Campaign to Campaign.<br>
            Socialite has no Instagram Certification and all mentions of Instagram or Instagram Logos/Trademarks on this website are property of Instagram.<br>
            <a href="/privacypolicy">Privacy Policy</a> • <a href="/termsofuse">Terms of Use</a> • <a href="/returnpolicy">Return Policy</a> • <a href="/disclaimer">Website Disclaimer

        </div>
    </div>

    <!-- Scripts -->
<script>
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
            document.getElementById("navbar").style.top = "0";
        } else {
            document.getElementById("navbar").style.top = "-121px";
        }
    }
    setTimeout(function() {
        var tidioScript = document.createElement('script');
        tidioScript.src = '//code.tidio.co/PUBLIC_KEY.js';
        document.body.appendChild(tidioScript);
    }, 5 * 500);
</script>
</body>
</html>

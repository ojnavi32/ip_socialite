@extends('layouts.app')
@section('content')
        <div class="content">
            <div class="contentsDiv">
                <div class="tbtext" style="min-height:121px;">
                </div>
                <div class="topimage" style="height:121px;">
                    <div class="color-overlay" style="background:#151515;position:relative;"></div>
                </div>
            </div>
            <div class="contentsDiv" style="font-family:'futura-pt'">
                <div class="twtextlp">
                    <h3 style="font-size:42px;text-align:center;font-weight:300;margin:20px auto;">Customer Reviews</h3>
                    <p style="text-align:center;font-size:21px;font-weight:300;">See the testimonials from Socialite Customers.</p><br><br><br>
                </div>
            </div>
            <div class="contentsDiv" style="font-family:'futura-pt'">
            <section class="desktab">
                <div class="container">
                    <div class="row justify-content-center" style="margin:30px auto;">
                        <div class="col-4 align-self-center" style="text-align:center;"><img src="/images/images17.png" style="width:100%;max-width:362px;margin-left:auto;"></div>
                        <div class="col-6 align-self-center">
                            <h5 style="font-size:18px;letter-spacing:4px;">@<strong>iamtimmytommy</strong><br></h5>
                            <h2 style="font-size:32px;"><em>Socialite has been nothing short of AMAZING! - 101k Followers</em><br></h2>
                            <p style="font-size:21px;">I’ve had an incredible experience with Socialite. They’ve give me more exposure and have connected me with the right people through targeted marketing.<br></p>
                        </div>
                    </div>
                    <div class="row justify-content-center" style="margin:30px auto;">
                        <div class="col-6 align-self-center">
                            <h5 class="text-right" style="font-size:18px;letter-spacing:4px;">@<strong>Stevencade</strong><br></h5>
                            <h2 class="text-right" style="font-size:32px;"><em>Excellent service! - 29k Followers</em><br></h2>
                            <p class="text-right" style="font-size:21px;">I’ve been in country music for a while and before I signed up with socialite, I was at 500 followers. Now I’m at 29k! About to hit 30k. In lieu of gaining a lot of followers my network has also increased dramatically, as well as my fan base. It’s pretty amazing.<br></p>
                        </div>
                        <div class="col-4" style="text-align:center;"><img src="/images/images18.png" style="width:100%;max-width:362px;margin-left:auto;"></div>
                    </div>
                    <div class="row justify-content-center" style="margin:30px auto;">
                        <div class="col-4" style="text-align:center;"><img src="/images/images19.png" style="width:100%;max-width:362px;margin-left:auto;"></div>
                        <div class="col-6 align-self-center">
                            <h5 style="font-size:18px;letter-spacing:4px;">@<strong>stylehacksbyazy</strong><br></h5>
                            <h2 style="font-size:32px;"><em>Absolutely worth the money! - 9k Followers</em><br></h2>
                            <p style="font-size:21px;">Socialite helped me gain traction on instagram. I wasn’t really sure where to start or how to gain followers but they came along side of me with a solution that solved my issue. They had amazing customer service as well and went over and above to make sure I was taken care of.<br></p>
                        </div>
                    </div>
                    <div class="row justify-content-center" style="margin:30px auto;">
                        <div class="col-6 align-self-center">
                            <h5 class="text-right" style="font-size:18px;letter-spacing:4px;">@<strong>POOPIESGRAM</strong><br></h5>
                            <h2 class="text-right" style="font-size:32px;"><em>Socialite really helped me build a foundation with a solid following! - 136k Followers</em><br></h2>
                            <p class="text-right" style="font-size:21px;">Socialite engages with other people in my same tribe and organically grows followers that specifically pertain to me and my business. As an influencer, when I first started out Socialite help me reach and define my audience so that I could grow rapidly, but  with proper engagement. When I first joined their service Socialite was a different company, they only serviced particular people/ clients with their systems. But they crushed it, and I know they will be incredible now that they’ve opened to the public.<br></p>
                        </div>
                        <div class="col-4" style="text-align:center;"><img src="/images/image23.png" style="width:100%;max-width:362px;margin-left:auto;margin-top:40px;"></div>
                    </div>
                </div>
            </section>
            <section class="tabtab">
                <div class="container">
                    <div class="row justify-content-center" style="margin:30px auto;">
                        <div class="col-5 align-self-center" style="text-align:center;"><img src="/images/images17.png" style="width:100%;max-width:362px;margin-left:auto;"></div>
                        <div class="col-7 align-self-center">
                            <h5 style="font-size:18px;letter-spacing:4px;">@<strong>iamtimmytommy</strong><br></h5>
                            <h2 style="font-size:32px;"><em>Socialite has been nothing short of AMAZING! - 101k Followers</em><br></h2>
                            <p style="font-size:21px;">I’ve had an incredible experience with Socialite. They’ve give me more exposure and have connected me with the right people through targeted marketing.<br></p>
                        </div>
                    </div>
                    <div class="row justify-content-center" style="margin:30px auto;">
                        <div class="col-7 align-self-center">
                            <h5 class="text-right" style="font-size:18px;letter-spacing:4px;">@<strong>THECOFFEESCHOOL</strong><br></h5>
                            <h2 class="text-right" style="font-size:32px;"><em>Excellent service! - 29k Followers</em><br></h2>
                            <p class="text-right" style="font-size:21px;">I’ve been in country music for a while and before I signed up with socialite, I was at 500 followers. Now I’m at 29k! About to hit 30k. In low of gaining a lot of followers my network has also increased dramatically, as well as my fan base. It’s pretty amazing.<br></p>
                        </div>
                        <div class="col-5" style="text-align:center;"><img src="/images/images18.png" style="width:100%;max-width:362px;margin-left:auto;"></div>
                    </div>
                    <div class="row justify-content-center" style="margin:30px auto;">
                        <div class="col-5" style="text-align:center;"><img src="/images/images19.png" style="width:100%;max-width:362px;margin-left:auto;"></div>
                        <div class="col-7 align-self-center">
                            <h5 style="font-size:18px;letter-spacing:4px;">@<strong>BIKINIAPRIL</strong><br></h5>
                            <h2 style="font-size:32px;"><em>Absolutely worth the money! - 9k Followers</em><br></h2>
                            <p style="font-size:21px;">Socialite helped me gain traction on instagram. I wasn’t really sure where to start or how to gain followers but they came along side of me with a solution that solved my issue. They had amazing customer service as well and went over and above to make sure I was taken care of.<br></p>
                        </div>
                    </div>
                    <div class="row justify-content-center" style="margin:30px auto;">
                        <div class="col-7 align-self-center">
                            <h5 class="text-right" style="font-size:18px;letter-spacing:4px;">@<strong>POOPIESGRAM</strong><br></h5>
                            <h2 class="text-right" style="font-size:32px;"><em>Socialite really helped me build a foundation with a solid following! - 136k Followers</em><br></h2>
                            <p class="text-right" style="font-size:21px;">Socialite engages with other people in my same tribe and organically grows followers that specifically pertain to me and my business. As an influencer, when I first started out Socialite help me reach and define my audience so that I could grow rapidly, but  with proper engagement. When I first joined their service Socialite was a different company, they only serviced particular people/ clients with their systems. But they crushed it, and I know they will be incredible now that they’ve opened to the public.<br></p>
                        </div>
                        <div class="col-5" style="text-align:center;"><img src="/images/image23.png" style="width:100%;max-width:362px;margin-left:auto;"></div>
                    </div>

                </div>
            </section>
            <section class="phonetab">
                <div class="container">
                    <div class="row justify-content-center" style="margin-bottom:50px;">
                        <div class="col-12 align-self-center" style="text-align:center;margin-bottom:25px;"><img src="/images/images17.png" style="width:100%;max-width:362px;margin-left:auto;"></div>
                        <div class="col-12 align-self-center">
                            <h5 class="text-center" style="font-size:18px;letter-spacing:4px;">@<strong>iamtimmytommy</strong><br></h5>
                            <h2 class="text-center" style="font-size:32px;"><em>Socialite has been nothing short of AMAZING! - 101k Followers</em><br></h2>
                            <p class="text-center" style="font-size:21px;">I’ve had an incredible experience with Socialite. They’ve give me more exposure and have connected me with the right people through targeted marketing.<br></p>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12" style="text-align:center;margin-bottom:25px;"><img src="/images/images18.png" style="width:100%;max-width:362px;margin-left:auto;"></div>
                        <div class="col-12 align-self-center" style="margin-bottom:50px;">
                            <h5 class="text-center" style="font-size:18px;letter-spacing:4px;">@<strong>THECOFFEESCHOOL</strong><br></h5>
                            <h2 class="text-center" style="font-size:32px;"><em>Excellent service! - 29k Followers</em><br></h2>
                            <p class="text-center" style="font-size:21px;">I’ve been in country music for a while and before I signed up with socialite, I was at 500 followers. Now I’m at 29k! About to hit 30k. In low of gaining a lot of followers my network has also increased dramatically, as well as my fan base. It’s pretty amazing.<br></p>
                        </div>
                    </div>
                    <div class="row justify-content-center" style="margin-bottom:50px;">
                        <div class="col-12" style="text-align:center;margin-bottom:25px;"><img src="/images/images19.png" style="width:100%;max-width:362px;margin-left:auto;"></div>
                        <div class="col-12 align-self-center">
                            <h5 class="text-center" style="font-size:18px;letter-spacing:4px;">@<strong>BIKINIAPRIL</strong><br></h5>
                            <h2 class="text-center" style="font-size:32px;"><em>Absolutely worth the money! - 9k Followers</em><br></h2>
                            <p class="text-center" style="font-size:21px;">Socialite helped me gain traction on instagram. I wasn’t really sure where to start or how to gain followers but they came along side of me with a solution that solved my issue. They had amazing customer service as well and went over and above to make sure I was taken care of.<br></p>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12" style="text-align:center;margin-bottom:25px;"><img src="/images/image23.png" style="width:100%;max-width:362px;margin-left:auto;"></div>
                        <div class="col-12 align-self-center" style="margin-bottom:50px;">
                            <h5 class="text-center" style="font-size:18px;letter-spacing:4px;">@<strong>POOPIESGRAM</strong><br></h5>
                            <h2 class="text-center" style="font-size:32px;"><em>Socialite really helped me build a foundation with a solid following! - 136k Followers</em><br></h2>
                            <p class="text-center" style="font-size:21px;">Socialite engages with other people in my same tribe and organically grows followers that specifically pertain to me and my business. As an influencer, when I first started out Socialite help me reach and define my audience so that I could grow rapidly, but  with proper engagement. When I first joined their service Socialite was a different company, they only serviced particular people/ clients with their systems. But they crushed it, and I know they will be incredible now that they’ve opened to the public.<br></p>
                        </div>
                    </div>
                </div>
            </section>
            </div>
            <br style="clear:both;">
            <div class="startb"><a href="/pricing">START NOW</a></div>
            <br style="clear:both;"><br><br><br>


        </div>
        <script>$('#carousel0').carousel()</script>
        <script>$('#carousel1').carousel()</script>

@endsection
@extends('layouts.app')
@section('content')
    <style>
        .card-body{
            font-family:"futura-pt";
            font-size:20px;
            font-weight:300;
        }
        .card-header{
            height:75px;
            position:relative;
        }
        .card-title{
            position:absolute;
            font-size:22px;
            height:80px;
            top:20px;
        }
        .boxed ul{
            width:100%;
            max-width:220px;
            margin: 0 auto;
        }
        .boxed {
            border:none;
            height:500px;
        }
        .startb{
            width:100%;
            max-width:220px;
            margin:0 auto;
        }
        .boxed h3{
            font-size:42px;
            font-weight:300;
        }
        .boxed h4{
            font-size:32px;
            font-weight:300;
        }
        .boxed h2{
            font-size:52px;
            font-weight:700;
        }
    </style>
        <div class="content">
            <div class="contentsDiv">
                <div class="tbtext" style="min-height:600px;">
                    <br><br><br><br><br><br>
                    <p class="fpt70">Buy Instagram Followers</p>
                    <p class="fpt36">FOR YOUR ACCOUNT</p>

                </div>
                <div class="topimage" style="height:593px;">
                    <div class="color-overlay" style="background:rgb(33,27,23);;position:relative;opacity:.45"></div>
                    <div class="imageback" style="background-image:url('/images/image9.jpg');background-position: center;opacity:0.45;"></div>
                </div>
            </div>
            <div class="contentsDiv">
                <div class="twtextlp">
                    <h3 style="font-size:44px;text-align:center;font-family:'futura-pt';font-weight:300">Choose From Any of the Following One-Time Plans for Your Budget & Goals.</h3>
                    <br><br>
                    <p style="text-align:center;font-size:21px;font-family:'europa';font-weight:300">With our packages you sign up once and we take care of everything else. Instantly start increasing your followers. We will never put your account at risk, guaranteed!</p><br><br>
                    <hr style="clear:both;">
                    <br><br>
                </div>
            </div>
            <div class="contentsDiv">
                <div class="twtextlp">
                    <div class="row no-pad">
                        <div class="coltab3b">
                            <div class="boxed">
                                <h5>STARTER KIT</h5>
                                <h2>1000</h2>
                                <h3>Followers</h3>
                                <h4>$30</h4>
                                <h5>One-Time Fee<br>3-6 Day Delivery Time</h5>
                                <br><br>
                                <div class="startb" ><a href="/startnow?plan=starter-kit-1000-followers">PURCHASE NOW</a></div>
                            </div>
                        </div>
                        <div class="coltab3b">
                            <div class="boxed">
                                <h5>RISING STAR</h5>
                                <h2>5000</h2>
                                <h3>Followers</h3>
                                <h4>$75</h4>
                                <h5>One-Time Fee<br>12-16 Day Delivery Time</h5>
                                <br><br>
                                <div class="startb" ><a href="/startnow?plan=rising-star-5000-followers">PURCHASE NOW</a></div>
                            </div>
                        </div>
                        <div class="coltab3b">
                            <div class="boxed">
                                <h5>INFLUENCER</h5>
                                <h2>10,000</h2>
                                <h3>Followers</h3>
                                <h4>$125</h4>
                                <h5>One-Time Fee<br>24-34 Day Delivery Time</h5>
                                <br><br>
                                <div class="startb" ><a href="/startnow?plan=influencer-10K-followers">PURCHASE NOW</a></div>
                            </div>
                        </div>
                    </div>
                    
                    <br style="clear:both;">
                    <div class="contentsDiv">
                        <h3 style="font-size:44px;text-align:center;font-family:'futura-pt';font-weight:300">Looking For A Custom Package?</h3>
                        <p style="text-align:center;">If you need a custom package put together please don’t hesitate to reach out to us here.</p>
                        <br><br>
                        <hr style="clear:both;">
                        <br><br>
                        <h3 style="font-size:44px;text-align:center;font-family:'futura-pt';font-weight:300">FAQ</h3><br>
                        <div class="sectionb" style="width:100%;max-width:600px;margin:0 auto;">

                    <div class="container" style="font-family:'futura-pt';font-size:28px;font-weight:500">
                        <div class="accordion indicator-plus-before round-indicator" id="accordionH" aria-multiselectable="true">
                            <div class="card m-b-0">
                                <div class="card-header collapsed" role="tab" id="headingOneH" href="#collapseOneH" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapseOneH">
                                    <a class="card-title">Why should I buy Instagram followers?</a>
                                </div>
                                <div class="collapse" id="collapseOneH" role="tabpanel" aria-labelledby="headingOneH">
                                    <div class="card-body">
                                    It’s a great way to get an Instagram accounts noticed. Not everyone has a huge advertising budget or a legion of followers so buying followers is the best way to get your account seen among the million of other accounts. According to statistics, an individual is 90% more likely to like your posts and follower your account if they see that others are doing the same. 
                                    </div>
                                </div>
                                <div class="card-header collapsed" role="tab" id="headingTwoH" href="#collapseTwoH" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapseTwoH">
                                    <a class="card-title">How do I choose a package?</a>
                                </div>
                                <div class="collapse" id="collapseTwoH" role="tabpanel" aria-labelledby="headingTwoH">
                                    <div class="card-body">
                                    Choosing a package is a relatively easy process. Simply decide how little or how many likes you want to receive to each post. We have clients that receive up to 50k followers or as little as 500 followers. It’s really up to you. Keep in mind that it will never be exactly that amount to look more natural. You can easily upgrade or downgrade your package at any time in the future. 
                                    </div>
                                </div>
                                
                                <div class="card-header collapsed" role="tab" id="headingThreeH" href="#collapseThreeH" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapseThreeH">
                                    <a class="card-title">Are bigger packages available?</a>
                                </div>
                                <div class="collapse" id="collapseThreeH" role="tabpanel" aria-labelledby="headingThreeH">
                                    <div class="card-body">
                                    Yes. With the prepaid follower packages, you can request higher follower amounts. Contact us at hello@socialite.online. We can create a custom package.

                                    </div>
                                </div>

                                <div class="card-header collapsed" role="tab" id="heading01H" href="#collapse01H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse01H">
                                    <a class="card-title">How do I cancel a subscription?</a>
                                </div>
                                <div class="collapse" id="collapse01H" role="tabpanel" aria-labelledby="heading01H">
                                    <div class="card-body">
                                    You can email us at any time and put your account on hold, or cancel it altogether.
                                    </div>
                                </div>
                                

                            </div>
                        </div>	
                    </div>


                </div>
            </div>
            <br stle="clear:both;"><br>
            <div class="contentsDiv">
                <div class="twtextlp" style="width:100%;max-width:600px;margin:0 auto;">
                    <h3 style="font-size:44px;text-align:center;font-family:'futura-pt';font-weight:300">Have Questions?</h3>
                    <br><br>
                    <p style="text-align:center;font-size:21px;font-family:'europa';font-weight:300">We have support specialists available who can answer any questions or concerns that you may have. Your feedback is important to us.</p><br><br>
                    <br><br>
                </div>
            </div>

                    </div>
                </div>
            </div>

@endsection
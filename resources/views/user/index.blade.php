@extends('layouts.dashboard')

@section('content')
<div class="row">
	<div class="box">
		<div class="box-header">
			<h3 class="title">Customer List</h3>
		</div>
		<div class="box-body">
			<table class="table table-striped table-hover">
				<thead>
					<th>NAME</th>
					<th>EMAIL</th>
					<th>PHONE</th>
				</thead>
				<tbody>
					@foreach ($users as $user)
					<tr>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->phone }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="box-footer">
			{{ $users->appends(request()->except('page'))->links() }}
		</div>
	</div>
</div>
@endsection
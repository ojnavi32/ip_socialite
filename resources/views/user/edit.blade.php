@extends('layouts.dashboard')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<form action="{{ url(sprintf('app/user/%s', $user->id)) }}" method="POST">
		        {{ csrf_field() }}
		        <input name="_method" type="hidden" value="PATCH">
		        <input name="user_id" type="hidden" value="{{ $user->id }}">
			<div class="box-header">
				<h3 class="title">Edit {{ $user->name }}</h3>
			</div>
			<div class="box-body">
				@include('user.partials.form')
			</div>
			<div class="box-footer">
				<button class="btn btn-block btn-primary">Submit</button>
			</div>
			</form>
		</div>
	</div>
</div>
@endsection
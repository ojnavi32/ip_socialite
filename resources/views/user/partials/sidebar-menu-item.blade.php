@can('manage-users')
  <li class="treeview {{
        Request::is('app/user') ||
        Request::is('app/user/*') ||
        Request::is('app/user/create') ||
        Request::is('app/user/*/edit') ? 'active open': ''
    }}"
  >
    <a href="">
      <i class="fa fa-users"></i>
      <span>Users</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li>
        <a href="{{ url('app/user') }}">
          <i class="fa fa-circle-o"></i> List
        </a>
      </li>
      <!-- <li class="{{ Request::is('users/create') ? 'active': ''}}">
        <a href="{{ url('users/create')}}">
          <i class="fa fa-plus"></i>
          {{ trans('app.add_new') }}
        </a>
      </li> -->
    </ul>
  </li>
@endcan

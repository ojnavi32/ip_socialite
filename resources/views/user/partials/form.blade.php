<div class="col-md-6">
	<label>Name <span class="required">*</span></label>
	<input type="text" 
		class="form-control" 
		name="name" 
		@if(isset($user['name']))
			value="{{ $user['name'] }}" 
		@else
			value="{{ old('name') }}" 
		@endif
		required
	>
</div>
<div class="col-md-6">
	<label>Email <span class="required">*</span></label>
	<input type="email" 
		class="form-control" 
		name="email" 
		@if(isset($user['email']))
			value="{{ $user['email'] }}" 
		@else
			value="{{ old('email') }}" 
		@endif
		required
	>
</div>
<div class="col-md-6">
	<label>Phone <span class="required">*</span></label>
	<input type="text" 
		class="form-control" 
		name="phone" 
		@if(isset($user['phone']))
			value="{{ $user['phone'] }}" 
		@else
			value="{{ old('phone') }}" 
		@endif
		required
	>
</div>
@extends('layouts.dashboard')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<a href="{{ url(sprintf('app/user/%s/edit', $user->id)) }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
			</div>
			<div class="box-body">
			<div class="card card-block sameheight-item">
		    	<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Name</label>
						<p class="form-control-static underlined">
							{{ $user->name }}
						</p>
					</div>
		    	</div>
		    	<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Email</label>
						<p class="form-control-static underlined">
							{{ $user->email }}
						</p>
					</div>
		    	</div>
		    	<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Phone Number</label>
						<p class="form-control-static underlined">
							{{ $user->phone }}
						</p>
					</div>
		    	</div>
		    	@can('manage-users')
		    	<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Status</label>
						<p class="form-control-static underlined">
							{{ ucwords($user->status) }}
						</p>
					</div>
		    	</div>
		    	@endcan
			</div>
			</div>
		</div>
	</div>
</div>
@endsection
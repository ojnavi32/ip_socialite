  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>L</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Socia</b>LITE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Notifications: style can be found in dropdown.less -->
          @can('view-notifications')
            <notify-admin></notify-admin>
          @endcan
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown profile">
            <a class="nav-link dropdown-toggle"
                data-toggle="dropdown"
                href="#" role="button"
                aria-haspopup="true"
                aria-expanded="false">
              <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
              <span class="name">{{ Auth::user()->name }}</span>
              <i class="fa fa-caret-down"></i>
            </a>
            <div class="dropdown-menu profile-dropdown-menu"
                aria-labelledby="dropdownMenu1">
                <a class="dropdown-item"
                    href="{{ url('app/user/profile') }}">
                    <i class="fa fa-user-circle-o icon"></i>
                    Profile
                </a>
                <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="{{ url('userLogout')}}">
                  <i class="fa fa-power-off icon"></i>
                  Logout
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
  </header>
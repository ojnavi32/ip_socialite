  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="Request::is('app') ? 'active' : ''">
          <a href="/app">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        @include('user.partials.sidebar-menu-item')
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
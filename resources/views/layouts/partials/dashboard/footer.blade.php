  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <!-- <b>Version</b> 2.4.0 -->
    </div>
    <strong>Copyright &copy; {{ date('Y') }} {{ env('APP_NAME') }}.</strong> All rights
    reserved.
  </footer>
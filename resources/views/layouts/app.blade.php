<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}"/>
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="stylesheet" href="https://use.typekit.net/gvl6bsk.css">
    <link rel="stylesheet" href="https://use.typekit.net/irw3hsg.css">
    <link rel="stylesheet" href="https://use.typekit.net/gbm0xte.css">
    <!-- Styles -->
    <link rel='stylesheet prefetch' href='/css/font-awesome.min.css'>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/style.css?ver=0.1" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="/js/jquery.min.js"></script>    
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script src="//code.tidio.co/hvutxhnh982rb2rxkllyc1vlbwqjybaz.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1640777426041031');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" 
    src="https://www.facebook.com/tr?id=1640777426041031&ev=PageView&noscript=1"/></noscript>
    <!-- End Facebook Pixel Code -->


    <!-- Global site tag (gtag.js) - Google Analytics --> 
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117831078-1"></script> 
    <script> 
        window.dataLayer = window.dataLayer || []; 
        function gtag(){dataLayer.push(arguments);} 
        
        gtag('js', new Date());
        
        gtag('config', 'UA-117831078-1');
    </script>


   <style>
        .container{
            margin:0px;
            width:100%;
            max-width:96%;
            position:relative;
        }
        .navbar-header{
            width:30%;
            max-width:390px;
            position:absolute;
            left:0px;
            top:-10px;
        }
        #app-navbar-collapse{
            position:absolute;
            right:0px;
            width:75%;
            top:-8px;
        }
        #menuFS{
            float:right;
        }
        .carousel-caption{
            top:45%;
            height:200px;
        }
        .backover{
            background-color:rgba(0, 0, 0, 0.1);
            width:80%;
            padding-top:30px;
            padding-bottom:30px;
            margin:auto;
        }
        .carousel-caption {
            top: 40%;
            bottom: auto;
        }
        .carousel-caption h3, .carousel-caption p{
            color:white;
        }
        .backover h3{
            font-size:18px;
        }
        .backover p{
            font-size:14px;
        }
        .twtextl h2{
            font-family:'futura-pt';
            font-weight:300;
            font-size:42px;
        }
        .twtextl h3{
            font-family:'futura-pt';
            font-weight:500;
            font-size:90px;
            color:white;
        }
        .twtextl .blackh3{
            font-family:'futura-pt';
            font-weight:100;
            font-size:42px;
            color:black;
            text-align:center;
        }
        .twtextl .whitep{
            font-family:'futura-pt';
            font-weight:300;
            font-size:21px;
            color:white;
        }

        .twtextl p{
            font-family:'futura-pt';
            font-size:100;
            font-size:18px;
        }
        .brkline{
            height:40px;
            width:70%;
            margin-left:15%;
            clear:both;
            border-top:2px solid #4A4A4A;
        }
        .brk{
            height:40px;
            width:70%;
            margin-left:15%;
            clear:both;
        }
        .boxed ul{
            margin-left:22px;
            text-align:left;
        }
        .boxed{
            font-family:'freight-sans-pro';
            border:2px solid #4A4A4A;
            color:#4A4A4A;
            height:900px;
            margin:50px 10px;
            padding:50px 10px;
        }
        .boxed h2{
            font-size:48px;
            font-weight:700;
            text-align:center;
        }
        .boxed h3{
            font-size:32px;
            font-weight:700;
            text-align:center;
        }
        .boxed h4, .boxed del{
            font-size:24px;
            font-weight:700;
            text-align:center;
            margin:20px 0px;
        }
        .boxed h5{
            font-size:21px;
            font-weight:300;
            text-align:center;
        }
        .boxed li{
            font-size:20px;
            font-weight:300;
        }
        .contentsDiv h2{
            font-size:42px;

        }
        .twtextlp img{
            width:378px;
            height:auto;
        }
        .fptmp{
            font-size:21px;
            font-weight:300;
        }
        .fptmh2{
            font-size:52px;
        }
        .fptmh3{
            font-weight:300;
            font-size:32px;
        }
        .fptmh4{
            font-size:24px;
        }
        .fptmh5{
            font-size:18px;
        }
        .row.no-pad {
            margin-right:0;
            margin-left:0;
        }
        .row.no-pad > [class*='col-'] {
            padding-right:0;
            padding-left:0;
        }
        .btnb{
            border: 0;
            border-radius: 0;
            background: #4C4C4C;
            -webkit-appearance: none;
            color:white;
            padding:15px 30px;
            letter-spacing:4px;
            font-size:16px;
            text-decoration:none;
        }
        .btnb :hover{
            color:gray;
            text-decoration:none!important;
        }
        .accordion{
            margin-left:50px;
        }
        .row{
            margin-left: 0px;
            margin-right: 0px;
        }

        /* accordion styles */
        .accordion .card-header {
        cursor: pointer;
        }
        .accordion.heading-right .card-title {
        position: absolute;
        right: 50px;
        }
        .accordion.indicator-plus .card-header:after {
        font-family: 'FontAwesome';
        content: "\f068";
        float: right;
        }
        .accordion.indicator-plus .card-header.collapsed:after {
        content: "\f067";
        }
        .accordion.indicator-plus-before.round-indicator .card-header:before {
        font-family: 'FontAwesome';
        font-size: 14pt;
        content: "\f056";
        margin-right: 10px;
        margin-top:10px;
        }
        .accordion.indicator-plus-before.round-indicator .card-header.collapsed:before {
        content: "\f055";
        color: #000;
        }
        .accordion.indicator-plus-before .card-header:before {
        font-family: 'FontAwesome';
        content: "\f068";
        float: left;
        }
        .accordion.indicator-plus-before .card-header.collapsed:before {
        content: "\f067";
        }
        .accordion.indicator-chevron .card-header:after {
        font-family: 'FontAwesome';
        content: "\f078";
        float: right;
        }
        .accordion.indicator-chevron .card-header.collapsed:after {
        content: "\f077";
        }
        .accordion.background-none [class^="card"] {
        background: transparent;
        }
        .accordion.border-0 .card {
        border: 0;
        }

        .col-6 h2 em, .col-12 h2 em, .col-7 h2 em{
            font-size:32px;
            font-style:italic;
            font-weight:500;
        }

        #navbar {
            background-color: #151515;
            position: fixed;
            top: -121px;
            width: 100%;
            display:block;
            height:121px;
            transition: top 0.3s;
            left:0px;
            padding-top:20px;
        }
        .navbar-hidden{
            position:fixed;
            left:3%;
        }
        .menu-hidden{
            position:fixed;
            right:3%;
            font-family: "futura-pt";
            font-weight:700;
            font-size:18px;
            font-weight: 600;
            letter-spacing: 3px;
            list-style: none;
            width:70%;
            min-width:750px;
        }
        .menu-hidden li{
            float:right;
            margin:26px 15px;
        }
        .menu-hidden li a{
            text-decoration:none;
            color:white!important;
            transition: .2s;
            white-space: none;
        }
        .menu-hidden li a:hover{
            color:#9c9999!important;
            background:none;
            transition: .2s;
        }

        /* Dropdown Button */
        .dropbtn {
            background:none;
            color: white;
            padding: 18px;
            font-size: 18px;
            letter-spacing:3px;
            border: none;
        }

        /* The container <div> - needed to position the dropdown content */
        .dropdown {
            position: relative;
            display: inline-block;
            margin-top:-8px;
        }

        /* Dropdown Content (Hidden by Default) */
        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #262626;
            color:white;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        /* Links inside the dropdown */
        .dropdown-content a {
            padding: 12px 16px;
            text-decoration: none;
            display: block;
            font-weight:700;
        }

        /* Change color of dropdown links on hover */
        .dropdown-content a:hover {
            background-color: #ddd;
            color:#9c9999;
        }

        /* Show the dropdown menu on hover */
        .dropdown:hover .dropdown-content {
            display: block;
        }

        /* Change the background color of the dropdown button when the dropdown content is shown */
        .dropdown:hover .dropbtn {
            color:#9c9999;
        }
        .navs1, .navs2div {
            display:none;
        }
        .twtextl.carousel{
            width:1020px;
            height:1020px;
        }
        .carousel-item img{
            width:1020px;
            height:auto;
        }
        .twtextl{
            width:100%;
        }

        #carousel0 .carousel-inner .carousel-item img{
            width:300px!important;
        }
        #carousel1{
            width:100%;
            max-width:1020px;
            margin:0 auto;
        }
        #carousel1 .carousel-inner .carousel-item img{
            width:100%;
            height:100%;
            max-width:1020px;
            max-height:1020px;
        }
        #desktab{
            display:block;
        }
        #phonetab{
            display:none;
        }
        .desktab{
            display:block;
        }
        .tabtab{
            display:none;
        }.phonetab{
            display:none;
        }
        .twtextl .ax{
            padding:35px 0px;
        }
        #theprocess{
            margin-top:50px;
            font-weight:300;
            font-size:90px!important;
        }
        #whatwewill, #whatwewill em{
            margin-top:60px;
            font-weight:300;
            font-size:70px!important;
        }
        #whatwewill em{
            font-style:italic;
            font-weight:500;
        }
        #whatwewilldo{
            font-size:70px;
            margin-top:50px;
        }
        #menuFS .startm a{
            display:block!important;
            color:red!important;
            height:100%!important;
        }
        #menuFS .startm a:hover{
            color:white!important;
        }
        .coltab3{
            width:32%;
        }
        .coltab3a{
            width:30%;
            margin:0px 3% 0px 0px;
        }
        .coltab3b{
            width:30%;
            margin:0px 3% 0px 0px;
        }
        .coltab4{
            width:24%;
        }
        .coltab4a{
            width:24.5%;
        }

        #targetAud h3{
            font-size:90px;
        }

        /* New fix for Safari */
        .columns > * {
        display: inline-block;
        width: 100%;
        }
        @media only screen and (max-width: 1465px){
            .menu-hidden, #menuFS {
                display:none;
            }
            .navs1 {
                display:block;
            }
            .navs1 li{
                float:right;
                margin: 8px 9px;
                letter-spacing:1px;
                color:#fff;
                list-style:none;
            }
            .navs1 li a{
                font-size:16px;
                font-weight:500;
                color:#fff;
            }
            .dropbtn, .navs1>a{
                font-size:16px;
                font-weight:500;
                padding:0px;
                letter-spacing:1px;
            }
            .navs1{
                margin-right:-30px;
                margin-top:6px;
            }
        }

        @media (min-width: 620px) and (max-width: 1100px) {
            body{
                width:100vw;
                max-width:1024px;
            }
            .navbar-left,.navbar-right {
                float: none !important;
            }
            .navbar-toggle {
                display: block;
                position:fixed;
                right:20px;
            }
            #app-navbar-collapse{
                width:170px;
                margin-right:70px;
                margin-top:2px;
                overflow:hidden;
                border-top: 0px solid gray;
                border-color:transparent;
                background:rgba(50,50,50,.75);
                position:absolute;
            }
            #navbar{
                display:none;
            }
            .navbar-fixed-top {
                top: 0;
                border-width: 0 0 1px;
            }
            .navbar-collapse.collapse {
                display: none!important;
            }
            .navbar-nav {
                float: none!important;
                margin-top: 7.5px;
            }
            .navbar-nav>li {
                float: none;
            }
            .navbar-nav>li>a {
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .collapse.in{
                display:block !important;
            }
            .navs1 li{
                clear:both;
            }
            .navs1 ul{
                margin-left:5px!important;
            }
            .navs1{
                margin-right:0px;
                margin-top:-30px;
            }
            .navbar-collapse{
                padding:0px;
            }
            .navbar-toggle{
                color:white;
                font-weight:700;
            }
            .navbar-default .navbar-toggle{
                width:50px;
                background:none;
            }
            .navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover {
                background:none;        
            }
            .dropdown-content{
                right:100px;
                top:25px;
            }
            #desktab{
                display:none;
            }
            #phonetab{
                display:block;
            }
            .texttitle .col-lg-4 {
                margin: 20px 0px;
            }

            #twtextlf{
                height:500px!important;
                width:100vw!important;
            }
            .texttitle{
                width:100%!important;
            }
            .twtextlp img{
                width:100vw;
                height:auto;
            }
            .desktab{
                display:none;
            }
            .tabtab{
                display:block;
            }.phonetab{
                display:none;
            }
            .texttitle .col-lg-4 {
                margin: 30px auto;
            }
            .coltab3a{
                width:98%;
            }

            .coltab4{
                width:49%;
            }
            .coltab4a{
                width:49%;
            }

        }
        @media (min-width: 500px) and (max-width: 900px) {
            .coltab3b{
                width:48%;
                margin: 0px auto;
            }
        }
        @media (min-width: 320px) and (max-width: 620px) {
            
            /* For mobile phones: */
            [class*="col-"] {
                width: 92%!important;
                max-width:92vw;
            }
            body{
                width:100vw!important;
                max-width:100vw;
            }
            #app{
                width:100vw!important;
                max-width:100vw;
            }
            .navbar-header{
                width:100%;
                position:absolute;
                max-width:510px;
                right:5px;
            }
            #app-navbar-collapse{
                margin-right:5px;
                overflow:hidden;
                float:right;
                margin-top:55px;
                background:rgba(50,50,50,.75);
            }
            .navs1{
                display:none
            }
            .navbar-header button{
                position:absolute;
                right:-15px;
            }
            .navbar-brand img{
                margin-left:-10px;
                width:170px;
            }
            .navbar-hidden img{
                margin-left:10px;
                width:170px;
            }
            #navbar{
                height:90px;
            }
            #app-navbar-collapse .navs1{
                display:block;
                text-align:right;
                margin-right:-10px;
            }
            .navbar-nav{
                display:none;
            }
            .dropdown-content{
                right:5px;
            }
            .navbar-toggle{
                right:10px;
            }
            .navbar-collapse{
                width:100%;
                max-width:190px;
            }
            .navbar-default .navbar-toggle{
                width:40px;
                background:none;
                color:white;
            }
            .navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover {
                background:none;        
            }
            .contentsDiv{
                width:94%;
                margin:0px auto;
            }
            .tbtext{
                margin-top:-40px;
            }
            .fpt70{
                font-size:50px;
            }
            .fpt36{
                font-size:25px;
            }
            .texttitle h3{
                margin-top:-30%;        
            }
            #mbchange{
                font-size:60px!important;
                margin-top:-30%;
                margin-right:10px;
            }
            .startb a{
                width:100%;
            }
            .texttitle{
                max-width:100vw;
            }
            .texttitle .col-lg-4{
                margin:20px auto;
            }
            .twtextl{
                width:100%!important;
                height:500px!important;
            }
            .twtextl p{
                width:95%!important;
                margin:0px auto;
            }
            .backover h3, .backover p{
                color:black;
            }
            .twtextl{
                width:100%!important;
            }
            .footer{
                width:100%;
                height:650px;
            }
            .footcont{
                width:96%!important;
            }
            .sectionb{
                width:100vw;
                margin:0 auto;
            }
            .accordion{
                margin:0px;
            }
            .card-header a{
                display:inline-block;
                font-size:19px;
                line-height:1em;
                height:auto!important;
                margin:auto 10px;
            }
            .card-header{
                height:80px!important;
            }
            #carousel1{
                width:100%;
                max-width:1020px;
            }
            #carousel1 .carousel-inner .carousel-item img{
                width:96%;
                height:auto;
                max-width:1020px;
                max-height:1020px;
            }
            .carousel-inner{
                width:96%;
                height:auto;
            }
            .carousel-inner a{
                height:100%
            }
            .texttitle .col-lg-4 {
                margin: 20px 0px;
            }
            .footcont img{
                width:280px
            }
            #desktab{
                display:none;
            }
            #phonetab{
                display:block;
                margin-top:-150px;
            }
            .desktab{
                display:none;
            }
            .tabtab{
                display:none;
            }.phonetab{
                display:block;
            }
            .carousel-item img{
                margin:0px auto;
            }
            .twtextl h3{
                font-family:'futura-pt';
                font-weight:500;
                font-size:32px;
                color:white;
            }
            #theprocess{
                margin-top:50px;
                font-size:45px!important;
            }
            #whatwewill{
                margin-top:50px;
                font-size:35px!important;
            }
            #whatwewill em{
                font-size:35px!important;
                font-style:italic;
                font-weight:500;
            }

            .coltab4{
                width:99%;
            }
            .coltab4a{
                width:99%;
            }
            .coltab3a{
                width:99%;
            }
            .coltab3b{
                width:99%;
            }
            #targetAud h3{
                font-size:68px;
            }
            #navbar a img{
                margin-top:-6px!important;
                margin-left:0px!important;
            }
            #fptmh5cust{
                font-size:16px!important;
                letter-spacing:4px!important;
                margin-top:30px;
            }
            /* body{
                background:black!important;
            } */
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <div id="navbar">
                    <a class="navbar-hidden" href="{{ url('/') }}">
                        <img src="/images/logowhite.png" alt="Socialite" style="width:280px;height:auto;margin-top: 10px;">
                    </a>
                    <ul class="navs1" style="margin-right:10px;margin-top:22px;">
                        @if (Auth::guest())
                            <li><div class="startm" style="width:130px;height:50px;padding-top:8px;margin-top:-10px;"><a href="/pricing">START NOW</a></div></li>
                            <li><a href="/login">LOGIN</a></li>
                        @else
                            <li class="dropdown">
                                <a href="/myaccount">MY ACCOUNT</a>
<!--
                                <a href="/myaccount" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">MY ACCOUNT</a>    
                                <ul class="dropdown-menu" role="menu">
                                    <li><span class="fa fa-user-circle"><span> <a href="/profile">Profile</a></li>
                                    <li><span class="fa fa-address-card"><span> <a href="/profile">Subscription</a></li>
                                    <li><span class="fa fa-cogs"><span> <a href="/profile">Settings</a></li>
                                    <li><span class="fa fa-window-close"><span> 
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
-->
                            </li>
                        @endif
                        <li class="dropdown">
                            <a href="/instagramlikes" class="dropbtn" >MORE SERVICES</a>
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="/instagramlikes">BUY LIKES</a>
                                <a class="dropdown-item" href="/instagramfollowers">BUY FOLLOWERS</a>
                            </div>
                        </li>                    
                        <li class="menuitems"><a href="/contact">SUPPORT</a></li>
                        <li class="menuitems"><a href="/reviews">REVIEWS</a></li>
                        <li class="menuitems"><a href="/pricing">PRICING</a></li>
                        <li class="menuitems"><a href="/howitworks">HOW IT WORKS</a></li>
                    </ul>

                    <ul class="menu-hidden">
                        @if (Auth::guest())
                            <li style="margin:16px 2px 10px 1px;"><div class="startm"><a href="/pricing">START NOW</a></div></li>
                            <li><a href="/login">LOGIN</a></li>
                        @else
                            <li class="dropdown" style="text-align:center;">
                            <a href="/myaccount" >MY ACCOUNT </a>

<!--
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">MY ACCOUNT </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><span class="fa fa-user-circle"><span> <a href="/profile">Profile</a></li>
                                    <li><span class="fa fa-address-card"><span> <a href="/profile">Subscription</a></li>
                                    <li><span class="fa fa-cogs"><span> <a href="/profile">Settings</a></li>
                                    <li><span class="fa fa-window-close"><span> 
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form2" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                                -->
                            </li>
                        @endif
                        <li class="dropdown" style="margin-top:8px;margin-left:-10px;">
                            <button href="/instagramlikes" class="dropbtn" >MORE SERVICES</button>
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="/instagramlikes">BUY LIKES</a>
                                <a class="dropdown-item" href="/instagramfollowers">BUY FOLLOWERS</a>
                            </div>
                        </li>
                        <li><a href="/contact">SUPPORT</a></li>
                        <li><a href="/reviews">REVIEWS</a></li>
                        <li><a href="/pricing">PRICING</a></li>
                        <li><a href="/howitworks">HOW IT WORKS</a></li>
                    </ul>
                    </div>
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="fa fa-angle-double-down"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="/images/logowhite.png" alt="Socialite" style="width:280px;height:auto;margin-top: 10px;">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse" style="border-color:transparent;">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul id="menuFS" class="nav">
                        <!-- Authentication Links -->
                        <li><a href="/howitworks">HOW IT WORKS</a></li>
                        <li><a href="/pricing">PRICING</a></li>
                        <li><a href="/reviews">REVIEWS</a></li>
                        <li><a href="/contact">SUPPORT</a></li>
                        <li class="dropdown">
                            <button href="/instagramlikes" class="dropbtn" >MORE SERVICES</button>
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="/instagramlikes">BUY LIKES</a>
                                <a class="dropdown-item" href="/instagramfollowers">BUY FOLLOWERS</a>
                            </div>
                        </li>

                        @if (Auth::guest())
                            <li><a href="/login">LOGIN</a></li>
                            <li><div class="startm"><a href="/pricing" >START NOW</a></div></li>
                        @else
                            <li class="dropdown" style="margin-top:0px;text-align:center;">
                            <a href="/myaccount">MY ACCOUNT</a>
<!--
                               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">MY ACCOUNT</a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><span class="fa fa-user-circle"><span> <a href="/profile">Profile</a></li>
                                    <li><span class="fa fa-address-card"><span> <a href="/profile">Subscription</a></li>
                                    <li><span class="fa fa-cogs"><span> <a href="/profile">Settings</a></li>
                                    <li><span class="fa fa-window-close"><span> 
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form3" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                                -->
                            </li>
                        @endif
                    </ul>
                    <ul class="navs1" style="">
                        @if (Auth::guest())
                            <li><div class="startm" style="width:130px;height:50px;padding-top:8px;margin-top:-10px;"><a href="/pricing">START NOW</a></div></li>
                            <li><a href="/login">LOGIN</a></li>
                        @else
                            <li class="dropdown" style="text-align:center;">

                            <a href="/myaccount">MY ACCOUNT</a>
                            
<!--
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> MY ACCOUNT </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><span class="fa fa-user-circle"><span> <a href="/profile">Profile</a></li>
                                    <li><span class="fa fa-address-card"><span> <a href="/profile">Subscription</a></li>
                                    <li><span class="fa fa-cogs"><span> <a href="/profile">Settings</a></li>
                                    <li><span class="fa fa-window-close"><span> 
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form4" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
-->
                            </li>
                        @endif
                        <li class="dropdown">
                            <a href="/instagramlikes" class="dropbtn" >MORE SERVICES</a>
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="/instagramlikes">BUY LIKES</a>
                                <a class="dropdown-item" href="/instagramfollowers">BUY FOLLOWERS</a>
                            </div>
                        </li>                    
                        <li class="menuitems"><a href="/contact">SUPPORT</a></li>
                        <li class="menuitems"><a href="/reviews">REVIEWS</a></li>
                        <li class="menuitems"><a href="/pricing">PRICING</a></li>
                        <li class="menuitems"><a href="/howitworks">HOW IT WORKS</a></li>
                    </ul>
 
                </div>
            </div>
        </nav>

        @yield('content')

        <div class="twtextl" id="twtextlf" style="height:243px;text-align:center;background:#F7F7F7;">
            <div class="texttitle" style="width:990px;margin:0 auto;z-index:150;padding-top:30px">

                <div class="col-lg-4 ">
                    <div class="startb"><a href="/howitworks">WHAT WE DO</a></div>
                </div>
                <div class="col-lg-4">
                    <div class="startr"><a href="/pricing">START NOW</a></div>
                </div>
                <div class="col-lg-4">
                    <div class="startb" style="width:234px;"><a href="/contact">CUSTOMER SERVICE</a></div>
                </div>
                <div class="col-lg-12" style="text-align:center;font-size:18px;margin-top:10px;font-family:'futura-pt';">
                    <br>
                    <p style="letter-spacing:3px;line-spacing:1.2em">MONEY BACK GUARANTEE<br></p>
                    <p style="font-family:'europa';margin-top:10px;">If your account doesn't see growth we will give you a refund!</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <br><br><br>
        <div class="footcont">
            <img src="/images/logowhite.png" alt="Socialite"><br><br><br>

            © 2017 The Socialite Technology LLC<br>

            Results may vary from Campaign to Campaign.<br>
            Socialite has no Instagram Certification and all mentions of Instagram or Instagram Logos/Trademarks on this website are property of Instagram.<br>
            <a href="/privacypolicy">Privacy Policy</a> • <a href="/termsofuse">Terms of Use</a> • <a href="/returnpolicy">Return Policy</a> • <a href="/disclaimer">Website Disclaimer

        </div>
    </div>

    <!-- Scripts -->
<script>
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
            document.getElementById("navbar").style.top = "0";
        } else {
            document.getElementById("navbar").style.top = "-121px";
        }
    }
    setTimeout(function() {
        var tidioScript = document.createElement('script');
        tidioScript.src = '//code.tidio.co/PUBLIC_KEY.js';
        document.body.appendChild(tidioScript);
    }, 5 * 500);
</script>
@stack('scripts')
</body>

</html>

@extends('layouts.app')
@section('content')
        <script src="https://js.chargebee.com/v2/chargebee.js" data-cb-site="socialite-test"> </script>
        <div class="content">
            <div class="contentsDiv">
                <div class="tbtext" style="min-height:520px;">
                    <br><br><br><br><br><br>
                    <p class="fpt70">OUR PLANS</p>

                </div>
                <div class="topimage" style="height:516px;">
                    <div class="color-overlay" style="background:rgb(33,27,23);;position:relative;opacity:.45"></div>
                    <div class="imageback" style="background-image:url('/images/original_558373975.jpg');background-position: center;opacity:0.45;"></div>
                </div>
            </div>
            <div class="contentsDiv">
                <div class="twtextlp">
                    <h3 style="font-size:44px;text-align:center;font-family:'futura-pt';font-weight:300">Choose from any of the following plans for your budget and goals.</h3>
                    <br><br>
                    <p style="text-align:center;font-size:21px;font-family:'europa';font-weight:300">If you need a custom campaign put together please don’t hesitate to reach out to us.</p><br><br>
                    <hr style="clear:both;">
                    <br><br>
                </div>
            </div>
            <div class="contentsDiv">
                <div class="twtextlp">
                    <div class="row no-pad">
                        <div class="col-lg-3 col-md-6">
                            <div class="boxed">
                                <h3>No Strings</h3>
                                <h5>Package</h5>
                                <h4>was <del>$39</del></h4>
                                <h2><span>$ </span>29.99<span>/week</span></h2>
                                <br>
                                <div class="brkline"></div>
                                <ul>
                                    <li>Dedicated Account Manager</li>
                                    <li>100% Secure</li>
                                    <li>Customized Campaign Running Around the Clock</li>
                                    <li>You Can Cancel Anytime</li>
                                </ul>
                                <br style="clear:both;">
                                <div class="col-lg-12" style="text-align:center;">
                                    <br stle="clear:both;">
                                    <div class="startrs">
                                    <a class="startrs" id="register" href="javascript:void(0)" data-cb-type="checkout" data-cb-plan-id="no-strings">Start Today </a>
                                    </div>
                                    <input class="btnb" type="button" id="register" value="Start Today" hidden>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="boxed">
                                <h3>Bread & Butter</h3>
                                <h5>Package</h5>
                                <h4>was <del>$129</del></h4>
                                <h2><span>$ </span>109<span>/month</span></h2>
                                <br>
                                <div class="brkline"></div>
                                <ul>
                                    <li>Dedicated Account Manager</li>
                                    <li>100% Secure</li>
                                    <li>Customized Campaign Running Around the Clock</li>
                                    <li>You Can Cancel Anytime</li>
                                </ul>
                                <br style="clear:both;">
                                <div class="col-lg-12" style="text-align:center;">
                                    <br stle="clear:both;">
                                    <div class="startrs">
                                    <a class="startrs" id="register" href="javascript:void(0)" data-cb-type="checkout" data-cb-plan-id="bread-and-butter"> Start Today </a>
                                    </div>
                                    <input class="btnb" type="button" id="register" value="Start Today" hidden>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="boxed">
                                <h3>Committed</h3>
                                <h5>Package</h5>
                                <h4>was <del>$389</del></h4>
                                <h2><span>$ </span>297</h2>
                                <h4>for 3 months</h4>
                                <br>
                                <div class="brkline"></div>
                                <ul>
                                    <li>Dedicated Account Manager</li>
                                    <li>100% Secure</li>
                                    <li>Customized Campaign Running Around the Clock</li>
                                    <li>3 Month Minimum</li>
                                    <li>Renewal Every 3 Months</li>
                                    <li>You Can Cancel Anytime</li>
                                </ul>
                                <br style="clear:both;">
                                <div class="col-lg-12" style="text-align:center;">
                                    <br stle="clear:both;">
                                    <div class="startrs">
                                    <a class="startrs" id="register" href="javascript:void(0)" data-cb-type="checkout" data-cb-plan-id="committed"> Start Today </a>
                                    </div>
                                    <input class="btnb" type="button" id="register" value="Start Today" hidden>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="boxed">
                            <h3>Professional</h3>
                            <h5>Package</h5>
                            <h4>was <del>$600</del></h4>
                            <h2><span>$ </span>497</h2>
                            <h4>for 6 months</h4>
                            <br>
                            <div class="brkline"></div>
                            <ul>
                                <li>Dedicated Account Manager</li>
                                <li>100% Secure</li>
                                <li>Customized Campaign Running Around the Clock</li>
                                <li>Weekly Report</li>
                                <li>6 Month Minimum</li>
                                <li>Renewal Every 6 Months</li>
                                <li>You Can Cancel Anytime</li>
                            </ul>
                            <br style="clear:both;">
                                <div class="col-lg-12" style="text-align:center;">
                                    <br stle="clear:both;">
                                    <div class="startrs">
                                    <a class="startrs" id="register" href="javascript:void(0)" data-cb-type="checkout" data-cb-plan-id="professional"> Start Today </a>
                                    </div>
                                    <input class="btnb" type="button" id="register" value="Start Today" hidden>
                                </div>
                        </div>
                    </div>
                    <br style="clear:both;"><br><br><br><br>
                    </div>
                </div>
            </div>
            <div class="twtextl">
                <h3>MONEY BACK GUARANTEE</h3>
                <br>
                <p style="font-size:24px;">If your account doesn't see growth we will give you a refund!</a>
                </p><br><br>
           </div>

            <div class="contentsDiv">
                <div class="twtextlp" style="clear:both;font-family:'europa';">
                    <hr style="clear:both;">
                    <br><br>
                    <h3 style="font-size:42px;text-align:center;">FAQ</h3>
                    <br>
                    <div class="col-md-6 col-sm-12">
                        + WHAT HAPPENS AFTER I’VE PAID?<br><br>
                        <span style="font-weight:300;">Your account manager will contact you directly to put to discuss your marketing campaign and put together a plan to achieve the results you’re looking for.</span>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        HOW QUICKLY WILL I START TO SEE RESULTS?<br><br>
                        <span style="font-weight:300;">While results will very from account to account, you will start seeing action on your profile relatively quickly. There are several factors to consider when checking your results such as your target audience, your starting number of followers/average likes etc. However at Socialite we make it easy for you by sending you a weekly report tracking your stats.</span>
                    </div>
                    <br style="clear:both;"><br><br>
                    <div class="col-md-6 col-sm-12">
                        WHAT IF I WANT TO CHANGE MY CAMPAIGN?<br><br>
                        <span style="font-weight:300;">Your account manager will be in touch with you on a weekly basis. Should you need any changes such as upgrading your account or changing your targeting they can absolutely help with that. Additionally your account manager will be available at your disposal to help at any time. Simply reach out to them.</span>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        + WHAT IF I WANT TO UPGRADE MY PLAN?<br><br>
                        <span style="font-weight:300;">No problem, we can absolutely do that for you. However there is a 72 hour change window for subscription changes but we will pro-rate your account with that amount towards the new plan.</span>
                    </div>
                    <br style="clear:both;"><br><br>
                    <h3 style="font-size:42px;text-align:center;"><a href="/contact" style="font-size:42px;text-decoration:none;color:rgba(31,31,31,.5)">Click Here</a> for More FAQ</h3>
                    <br style="clear:both;"><br><br>
                </div>
            </div>
<script src="https://js.chargebee.com/v2/chargebee.js" data-cb-site="socialite-test"> </script>
<script>
    $(document).ready(function(){
        var randomstring = Math.random().toString(36).slice(-8);
        $('#pass').val(randomstring);

    });
</script>
@endsection
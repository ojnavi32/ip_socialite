@extends('layouts.app')
@section('content')
        <style>
            .h3-black-thin{
                font-size:32px!important;
                font-weight:300!important;
                color:black!important;
            }
            .h2-black-thin{
                font-size:42px!important;
                font-weight:300!important;
                color:black!important;
            }
            .h2-black-bold{
                font-size:42px!important;
                font-weight:700!important;
                color:black!important;
            }
            .imageback{
                width:100%;
            }
        </style>
        <div class="content">
            <div class="contentsDiv">
                <div class="tbtext">
                    <br><br><br><br>
                    <p class="fpt36">SEE YOUR INSTAGRAM SURGE</p>
                    <br>
                    <p class="fpt70">SOCIAL INFLUENCERS START WITH US.</p>
                    <br>
                    <p class="fpt36">SOCIALITE WILL EXPAND YOUR REACH THROUGH TARGET INTERACTIONS, INCREASE BRAND AWARENESS AND GROW YOUR FOLLOWERS.</p>
                    <br>
                    <div class="startw"><a href="/pricing">START NOW</a></div>
                </div>
                <div class="topimage">
                    <div class="color-overlay" style="background:rgb(33,27,23);;position:relative;opacity:.45"></div>
                    <div class="imageback" style="background-image:url('/images/image15.jpg');background-position: center;opacity:0.45;"></div>
                </div>
                <div class="twtext" style="font-size:32px;font-weight:600;font-style:italic;"><br><br>
                    "Socialite has been incredible. They have been very helpful with everything. Not to mention the excellent customer service. From day one my following has skyrocketed, my engagement has been amazing and they still continue to surprise me everyday. Absolutely worth the money."<br>
                    <span class="twtextuser">- LEILANI</span> <span class="twtextmonth">/  USER FOR 1 YEAR</span><br><br>
                    <hr class="breaker"/>
                    <br>

                </div>
            </div>
            <div class="contentsDiv">
                <div class="twtextl">
                    <h3 class="h3-black-thin">Why Work with Socialite?</h3>
                    <br><br>
                    <p>We manage your Instagram interactions with your target audience to get you the results you want. With a dedicated account manager you will get first class service at a price anyone can afford.</p><br><br><br>
                </div>
            </div>
            <div class="contentsDiv">
                <section id="desktab">
                    <div class="container" style="width:100%;">
                        <div class="row float-lg-none justify-content-center">
                            <div class="coltab3">
                                <div class="row row-underline" style="margin-top:35px;height:250px;margin-left:-15px;">
                                    <div class="col">
                                        <h1 class="text-right" style="font-size:21px;"><strong>VISIBLE RESULTS</strong><br></h1>
                                        <p class="text-right" style="font-size:21px;"><br>Within hours of starting your campaign the results begin coming in!<br><br></p>
                                    </div>
                                    <div class="col-lg-3"><img src="/images/Icon2.png"></div>
                                </div>
                                <div class="clearfix" style="height:120px;"></div>
                                <div class="row row-underline" style="height:210px;">
                                    <div class="col">
                                        <h1 class="text-right" style="font-size:21px;"><strong>VISIBLE RESULTS</strong><br></h1>
                                        <p class="text-right" style="font-size:21px;"><br>Within hours of starting your campaign the results begin coming in!<br><br></p>
                                    </div>
                                    <div class="col-lg-3"><img src="/images/Icon3.png"></div>
                                </div>
                            </div>
                            <div class="coltab3"><img src="/images/NewHeaderImage.png" style="width:100%;height:auto;margin-top:40px;"></div>
                            <div class="coltab3">
                                <div class="row row-underline" style="margin-top:35px;height:250px;">
                                    <div class="col-lg-3"><img src="/images/Icon1.png" style="margin-left:-30px;"></div>
                                    <div class="col">
                                        <h1 class="text-left" style="font-size:21px;"><strong>GENUINE</strong><br></h1>
                                        <p class="text-left" style="font-size:21px;margin-bottom:0px;"><br>Results are real and verifiable, no fake followers or likes. Real results, real organic growth.<br><br></p>
                                    </div>
                                </div>
                                <div class="clearfix" style="height:120px;"></div>
                                <div class="row row-underline" style="height:210px;">
                                    <div class="col-lg-3"><img src="/images/Icon4.png"style="margin-left:-30px;"></div>
                                    <div class="col">
                                        <h1 class="text-left" style="font-size:21px;"><strong>SIMPLE</strong><br></h1>
                                        <p class="text-left" style="font-size:21px;"><br>No software installations, downloads or learning to code. We do it all for you!<br><br></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="phonetab">
                    <div class="container" style="width:100%;">
                        <div class="row float-lg-none justify-content-center">
                            <div class="col-10 leftwhywork">
                                <div class="row row-underline" style="margin-top:35px;height:250px;margin-left:-15px;">
                                    <div class="col-6 offset-3 m-auto" style="text-align:center;"><img src="/images/Icon2.png" style="margin:0 auto;"></div>
                                    <div class="col-12" style="margin-top:30px;">
                                        <h1 class="text-center" style="font-size:21px;"><strong>VISIBLE RESULTS</strong><br></h1>
                                        <p class="text-center" style="font-size:21px;"><br>Within hours of starting your campaign the results begin coming in!<br><br></p>
                                    </div>
                                </div>
                                <div class="clearfix" style="height:120px;"></div>
                                <div class="row row-underline" style="height:210px;">
                                    <div class="col-6 offset-3" style="text-align:center;"s><img src="/images/Icon3.png"></div>
                                    <div class="col-12" style="margin-top:30px;">
                                        <h1 class="text-center" style="font-size:21px;"><strong>VISIBLE RESULTS</strong><br></h1>
                                        <p class="text-center" style="font-size:21px;"><br>Within hours of starting your campaign the results begin coming in!<br><br></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-10" style="margin-top:125px;"><img src="/images/NewHeaderImage.png" style="width:100%;height:auto;"></div>
                            <div class="col-10">
                                <div class="row row-underline" style="margin-top:35px;height:350px;">
                                    <div class="col-6 offset-3" style="text-align:center;"><img src="/images/Icon1.png"></div>
                                    <div class="col-12" style="margin-top:30px;">
                                        <h1 class="text-center" style="font-size:21px;"><strong>GENUINE</strong><br></h1>
                                        <p class="text-center" style="font-size:21px;margin-bottom:0px;"><br>Results are real and verifiable, no fake followers or likes. Real results, real organic growth.<br><br></p>
                                    </div>
                                </div>
                                <div class="clearfix" style="height:120px;"></div>
                                <div class="row row-underline" style="height:350px;">
                                    <div class="col-6 offset-3" style="text-align:center;"><img src="/images/Icon4.png"></div>
                                    <div class="col-12" style="margin-top:30px;">
                                        <h1 class="text-center" style="font-size:21px;"><strong>SIMPLE</strong><br></h1>
                                        <p class="text-center" style="font-size:21px;"><br>No software installations, downloads or learning to code. We do it all for you!<br><br></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-6 offset-3 align-self-center" style="margin-top:71px;"><div class="startb"><a href="/pricing">START NOW</a></div>
                                <p
                                    class="text-center" style="font-size:24px;"><br>MONEY BACK GUARANTEE<br></p>
                                    <p class="text-center" style="font-size:22px;">If your account doesn't see growth we will give you a refund!<br><br></p>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
            <div class="twtextl" style="width:100%;height:441px!important;position:relative;display: table;">
                <div class="texttitle" id="targetAud" style="width:95%;position:absolute;z-index:150;height:100%;display: inline-flex!important; justify-content: center; align-items: center; ">
                    <h3 style="color:white;font-family:'futura-pt';font-weight:500;">Target Your Audience</h3>
                </div>
                <div class="color-overlay"></div>
                <div class="imageback" style="background-image:url('/images/images17.jpg');"></div>
            </div>
            <div class="contentsDiv">
                <div class="twtextl">
                    <br><br><br>
                    <h3 class="h3-black-thin">Target Your Audience</h3>
                    <br><br>
                    <p>These are some of the demographics we can target for you</p><br><br><br>
                </div>
                <div class="twtextl" style="width:300px;margin:0px auto;">

                    <div id="carousel0" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="/images/cp2.png" alt="">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/cp3.png" alt="">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/cp4.png" alt="">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/cp5.png" alt="">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/cp6.png" alt="">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/cp7.png" alt="">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/cp8.png" alt="">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/cp9.png" alt="">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/cp10.png" alt="">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/cp11.png" alt="">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/images/cp12.png" alt="">
                            </div>
                    </div>
                    </div>
                    </div>
                    <br><br><br>
                    <div class="startb"><a href="/pricing">START NOW</a></div>
                    <br><br><br>
                </div>

            <div class="twtextl" style="width:100vw;height:518px;position:relative;">
                <div class="texttitle" style="width:100%;position:absolute;margin-top:170px;text-align:center;z-index:150;"><h3 id="mbchange" style="color:white;font-size:70px;font-family:'futura-pt';">What People Are Saying About<br>Socialite</h3></div>
                <div class="color-overlay"></div>
                <div class="imageback" style="background-image:url('/images/image4.jpg');"></div>
            </div>
            <br><br><br>

            <div class="contentsDiv">
                <div class="twtextl">
                    <h3 class="h2-black-thin" style="">What People Are Saying About <span class="h2-black-bold">Socialite</span>™</h3>
                    <br><br>
                    <p style="letter-spacing:4px;">SEE MORE REVIEWS <a href="/testimonials" style="text-decoration:none;color:gray;"><span style="font-weight:900;font-size:20px;">HERE</a></span></p><br><br><br>
                    
                </div>
            </div>
            <div class="twtextl">

                <div id="carousel1" class="carousel carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="/images/images17.png" alt="">
                            <div class="carousel-caption d-none d-md-block">
                                <div class="backover"><h3><strong>@iamtimmytommy</strong></h3>
                                <p><strong>Followers - 101k<strong><br>
                                I’ve had an incredible experience with Socialite. They’ve give me more exposure and have connected me with the right people through targeted marketing.</p></div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="/images/images18.png" alt="">
                            <div class="carousel-caption d-none d-md-block">
                                <div class="backover"><h3><strong>@Stevencade</strong></h3>
                                <p><strong>Followers - 29k<strong><br>
                                I’ve been in country music for a while and before I signed up with socialite, I was at 500 followers. Now I’m at 29k! About to hit 30k. In low of gaining a lot of followers my network has also increased dramatically, as well as my fan base. It’s pretty amazing.</p></div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="/images/images19.png" alt="">
                            <div class="carousel-caption d-none d-md-block">
                                <div class="backover"><h3><strong>@stylehacksbyazy</strong></h3>
                                <p><strong>Followers - 9k<strong><br>
                                Socialite helped me gain traction on instagram. I wasn’t really sure where to start or how to gain followers but they came along side of me with a solution that solved my issue. They had amazing customer service as well and went over and above to make sure I was taken care of.</p></div>
                            </div>
                        </div>
                        
                        <div class="carousel-item" style="margin-top:-10px;">
                            <img class="d-block w-100" src="/images/image23.png" alt="">
                            <div class="carousel-caption d-none d-md-block" style="margin-top:-10px;">
                                <div class="backover"><h3><strong>@poopiesgram</strong></h3>
                                <p><strong>Followers - 136k<strong><br>
                                Socialite engages with other people in my same tribe and organically grows followers that specifically pertain to me and my business. As an influencer, when I first started out Socialite help me reach and define my audience so that I could grow rapidly, but  with proper engagement...</p></div>
                            </div>
                        </div>



                        <a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="twtextl" style="height:180px!important;display:block;margin-top:130px;">
                <div class="startb"><a href="/pricing">START NOW</a></div>
            </div>
        </div>
@include('flash.ourSale')
        <script>$('#carousel0').carousel()</script>
        <script>$('#carousel1').carousel()</script>
@endsection

@push('scripts')
<script>
    $(function () {
        setTimeout(function() {
            $('#myModal').modal();
            $('#myModal').modal({backdrop: 'static', keyboard: false});
        }, 10000);
        // $('#myModal').modal('show');
    });
</script>
@endpush
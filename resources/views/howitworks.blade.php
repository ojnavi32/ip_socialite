@extends('layouts.app')
@section('content')
        <style>
            .iconimg{
                width:90%;
                height:80px;
                margin:0px auto;
            }
        </style>
        <div class="content">
            <div class="contentsDiv">
                <div class="tbtext" style="min-height:700px;">
                    <br><br><br><br>
                    <p class="fpt70">How Does Socialite Work?</p>
                    <br>
                    <p class="fpt36">GENERAL ANSWERS AND GUIDE TO EASY INSTAGRAM GROWTH</p>
                    <br>
                </div>
                <div class="topimage" style="height:685px;">
                    <div class="color-overlay" style="background:rgb(33,27,23);;position:relative;opacity:.45"></div>
                    <div class="imageback" style="background-image:url('/images/image8.jpg');background-position: center;opacity:0.45;"></div>
                </div>
                <div class="twtextl">
                    <h4 class="blackh3">How Does Socialite Work?</h4>
                    <br><br>
                    <p>Socialite assists you in the natural growth of<br>
                    your Instagram followers, using advanced targeting and<br>
                    managing the interactions with your target<br>
                    audience. The results get you the type of connections<br>
                    that you actually want and need. We work around the<br>
                    clock to make growing your Instagram network painless.<br>
                    <a href="/startnow" style="text-decoration:none;color:gray;">Start your Socialite campaign now.</a>
                    </p>
                </div>
            </div>
            <div class="contentsDiv">
            <section>
                <div class="row" style="margin-top:80px;">
                    <div class="coltab4">
                        <div class="row">
                            <div class="col-12" style="text-align:center;margin-bottom:20px;"><img src="/images/Artboard7.png" style="text-align:center;"></div>
                            <div class="col-12 m-auto">
                                <p class="text-center">SIMPLE PROCESS<br></p>
                                <p class="text-center">It's easy to get started. Just fill in your information on our start now page and you'll have taken the first step towards setting up your campaign.<br></p>
                            </div>
                        </div>
                    </div>
                    <div class="coltab4">
                        <div class="row">
                            <div class="col-12" style="text-align:center;margin-bottom:20px;"><img src="/images/Artboard6.png"></div>
                            <div class="col-12">
                                <p class="text-center">ACCOUNT MANAGEMENT<br></p>
                                <p class="text-center">Every client we work with has an account manager that will make sure your campaign is always running smoothly. They will be available to discuss your campaign before it starts if you like as well as at any point throughout the
                                    process for any needed changes.<br><br></p>
                            </div>
                        </div>
                    </div>
                    <div class="coltab4">
                        <div class="row">
                            <div class="col-12" style="text-align:center;margin-bottom:20px;"><img src="/images/Artboard4.png" style="text-align:center;"></div>
                            <div class="col-12">
                                <p class="text-center">ADVANCED TARGETING SYSTEM<br></p>
                                <p class="text-center">We use a variety of proven techniques to target genuine interactions with your principle audience to help grow your account and build true fans!<br></p>
                            </div>
                        </div>
                    </div>
                    <div class="coltab4">
                        <div class="row">
                            <div class="col-12" style="text-align:center;margin-bottom:20px;"><img src="/images/Artboard5.png" style="margin:0 auto;"></div>
                            <div class="col-12">
                                <p class="text-center">ON DEMAND&nbsp;<br>REPORTING<br></p>
                                <p class="text-center">Anytime you'd like your account manager can send you a report on the growth of your account so that you can analyze and make the necessary adjustments to your campaign.<br><br><br></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </div>

            <div class="contentsDiv" style="display:block;height:auto!important;">
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col" style="background-size:cover;background-repeat:no-repeat;background-position:center;margin:auto 0px;height:238px;background-image:url(&quot;/images/image20.jpg&quot;);">
                                <div style="margin-top:20px;">
                                    <h1 id="theprocess" class="text-center" style="color:rgb(255,255,255);font-size:90px;">The Process<br></h1>
                                    <p class="text-center" style="color:rgb(255,255,255);font-size:22px;">FOUR STEPS TO BECOMING AN INFLUENCER<br></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="twtextl" style="clear:both;">
                    <br><br><br>
                    <hr style="clear:both;">
                    <br><br>
                    <p style="width:350px;">We manage these interactions for you on a massive scale so that you can grow your following, reach your target audience and achieve social media success.</p>

                </div>
            </div>
            <div class="contentsDiv">
                
            <section>
                        <div class="row">
                            <div class="coltab4">
                                <div class="row">
                                    <div class="col-12" style="text-align:center;margin-bottom:20px;">
                                        <h1>1</h1>
                                    </div>
                                    <div class="col-12 m-auto">
                                        <p class="text-center">Choose your plan and checkout<br></p>
                                    </div>
                                </div>
                            </div>
                            <div class="coltab4">
                                <div class="row">
                                    <div class="col-12" style="text-align:center;margin-bottom:20px;">
                                        <h1>2</h1>
                                    </div>
                                    <div class="col-12">
                                        <p class="text-center">Within 24 hours of your plan selection we will begin setting up your campaign and will contact you if necessary to gather any additional information. If you'd like to speak with your account manager directly you can simply request
                                            a phone call and they're available at all times via email.&nbsp;<br><br></p>
                                    </div>
                                </div>
                            </div>
                            <div class="coltab4">
                                <div class="row">
                                    <div class="col-12" style="text-align:center;margin-bottom:20px;">
                                        <h1>3</h1>
                                    </div>
                                    <div class="col-12">
                                        <p class="text-center">You're account manager will setup your campaign and monitor it for you regularely. They will be reachable by email&nbsp; for any changes or tweaks.<br></p>
                                    </div>
                                </div>
                            </div>
                            <div class="coltab4">
                                <div class="row">
                                    <div class="col-12" style="text-align:center;margin-bottom:20px;">
                                        <h1>4</h1>
                                    </div>
                                    <div class="col-12">
                                        <p class="text-center">At anytime you can request a report documenting your progress.<br></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <br><br><br>
                    <div class="startb"><a href="/pricing">START NOW</a></div>
                    <br style="clear:both;"><br><br>


            </div>
            <div class="contentsDiv">
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col" style="background-size:cover;background-repeat:no-repeat;background-position:top;margin:auto 0px;height:274px;background-image:url(&quot;/images/images20.jpg&quot;);">
                                <div>
                                    <h1 id="whatwewill" class="text-center" style="color:rgb(255,255,255);">What We Will <em>Not </em>Do<br></h1>
                                    <p class="text-center" style="color:rgb(255,255,255);font-size:22px;">FOUR STEPS TO BECOMING AN INFLUENCER<br></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="contentsDiv" style="display:block;margin-top:50px;">
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="coltab3a">
                                <p class="text-center">FAKE FOLLOWERS<br></p>
                                <p class="text-center">Our Social Sensational organic growth packages will never provide fake followers.<br></p>
                                <p class="text-center">Only buy fake followers and likes if you need a quick boost. It can help in the short term but long term you need organic results<br></p>
                                <p class="text-center">All followers you’ll ever receive through your Social Sensational campaign with us are 100% genuine, active followers.<br></p>
                            </div>
                            <div class="coltab3a">
                                <p class="text-center">FOLLOW &amp; UNFOLLOW<br></p>
                                <p class="text-center">Many people have used the follow/unfollow technique to grow their Instagram in the past which consists of following a user hoping that they follow you back and then unfollowing them shortly after. If you want to grow your brand and
                                    create genuine interactions between yourself and your followers this technique will never work and we do not and will not do this ever.<br></p>
                            </div>
                            <div class="coltab3a">
                                <p class="text-center">GET YOU BANNED<br></p>
                                <p class="text-center">Our service is 100% based on proven marketing techniques. You have no risk of being banned by Instagram or any of the other platforms we cover.<br></p>
                            </div>
                        </div>
                    </div>
                </section>
                <br><br>
                <div class="startb"><a href="/pricing">START NOW</a></div>
                <br><br><br>

            </div>           
        </div>
        <script>$('#carousel0').carousel()</script>
        <script>$('#carousel1').carousel()</script>

@endsection
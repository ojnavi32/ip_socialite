@extends('layouts.app')
@section('content')
<style>
    .form-group p{
        font-weight:300;
        font-size:21px;
    }
</style>
<?php

        $post_data = array(
            'email'=>Auth::user()->email, 
            'instagram_name'=>Auth::user()->instagramname, 
            'instagram_password'=>Auth::user()->instampassword, 
            'campaign'=>Auth::user()->campaign,
            'phone_number'=>Auth::user()->phone,
            'status'=>'1'
        );
            
        pushMauticForm($post_data, 2, $_SERVER["REMOTE_ADDR"]);
        
        
        function pushMauticForm($data, $formId, $ip = null){
            // Get IP from $_SERVER
            if (!$ip) {
                $ipHolders = array(
                    'HTTP_CLIENT_IP',
                    'HTTP_X_FORWARDED_FOR',
                    'HTTP_X_FORWARDED',
                    'HTTP_X_CLUSTER_CLIENT_IP',
                    'HTTP_FORWARDED_FOR',
                    'HTTP_FORWARDED',
                    'REMOTE_ADDR'
                );
                foreach ($ipHolders as $key) {
                    if (!empty($_SERVER[$key])) {
                        $ip = $_SERVER[$key];
                        if (strpos($ip, ',') !== false) {
                            // Multiple IPs are present so use the last IP which should be the most reliable IP that last connected to the proxy
                            $ips = explode(',', $ip);
                            array_walk($ips, create_function('&$val', '$val = trim($val);'));
                            $ip = end($ips);
                        }
                        $ip = trim($ip);
                        break;
                    }
                }
            }
            
            $data['formId'] = $formId;
            // return has to be part of the form data array
            if (!isset($data['return'])) {
                $data['return'] = 'http://socialitedev.kohlkreative.com/myaccount';
            }
        
            $data = array('mauticform' => $data);
            // Change [path-to-mautic] to URL where your Mautic is
            $formUrl =  'http://socialitedev.kohlkreative.com/mautic/form/submit?formId=' . $formId;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $formUrl);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Forwarded-For: $ip"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
        
        }

?>
<div class="content">
    <div class="contentsDiv">
        <div class="tbtext" style="min-height:121px;">
        </div>
        <div class="topimage" style="height:121px;">
            <div class="color-overlay" style="background:#151515;position:relative;"></div>
        </div>
    </div>
    <div class="contentsDiv" style="font-family:'futura-pt';font-weight:300;">
        <h2 style="text-align:center;">Thank You!</h2><br><br>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col" style="height:700px;">
                        <div class="form-group" style="font-style:italic;padding:10px;">
                            <p><strong>Thank You! </strong>We will be in touch shortly. It takes 12-24 hours to setup your account. Don't worry, you're on your way!</p><br>
                            <p>**These should be comments that will work with almost any Instagram picture. A lot of people will use Emoji's or comments like "That's awesome", "Keep up the good work", "I love your content" etc. You probably get the point now so just put your own spin on it. Also if you need any help setting up your campaign you can email or call us.</p>
                            <p>Please check your email for your Socialite Account Information. If you want to change your password <a href="/changePassword">please here</a></p>
                        </div>
                    </div>
                    <div class="col" style="background-image:url(&quot;/images/download.jpg&quot;);background-size:cover;background-repeat:no-repeat;background-position:center;margin:0px 40px;"></div>
                </div>
                <br><br><hr>
                <div class="row" style="margin-top:50px;">
                    <div class="col">
                        <h3 class="text-center">What we need you to do.</h3>
                        <p class="text-center">To make sure your campaign is setup as quickly as possible, please go into your instagram settings and turn off Two-Factor Authentication. Instructions to do this can be found here. The reason we need to do this: With two factor authentication
                            on, every time your account manager logs into your account setup or work on your, you will be sent a security code and then need to send that to your account manager to grant access. Turning off Two Factor Authentication eliminates this extra steps and allows your account manager to work on your campaign hindrance free.</p>
                    </div>
                </div>
            </div>
        </section>
        <br><br> 
    </div>
</div>

@endsection
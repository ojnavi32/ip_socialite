@extends('layouts.app')
@section('content')

    <div class="content">
        <div class="contentsDiv">
            <div class="tbtext" style="min-height:121px;">
            </div>
            <div class="topimage" style="height:121px;">
                <div class="color-overlay" style="background:#151515;position:relative;"></div>
            </div>
        </div>
        <div class="contentsDiv" style="font-family:'futura-pt';font-weight:300;text-align:center;">
            <div style="text-align:center;">
                <h2 style="text-align:center;">Message Sent!</h2><br><br>
                <p style="font-size:28px;">We will be in touch shortly.</p>
            </div>
        </div>
        <br style="clear:both;">
        <br style="clear:both;"><br><br><br>

    </div>


@endsection
@extends('layouts.app')
@section('content')
        <style>
        .btnblack a:hover{
            color:gray;
            text-decoration:none!important;
        }
        </style>
        <div class="content" style="font-family:futura-pt;">
            <div class="contentsDiv">
                <div class="tbtext" style="min-height:520px;">
                    <br><br><br><br><br><br>
                    <p class="fpt70">Start Now</p>

                </div>
                <div class="topimage" style="height:516px;">
                    <div class="color-overlay" style="background:rgb(33,27,23);;position:relative;opacity:.45"></div>
                    <div class="imageback" style="background-image:url('/images/image13.jpg');background-position: top;opacity:0.45;"></div>
                </div>
            </div>
            <div class="contentsDiv">
                <div class="twtextlp">
                    <h3 style="font-size:42px;text-align:center;">GET STARTED!</h3>
                    <br><br>
                    <p style="text-align:center;font-size:22px;font-weight:300;">Easily start growing your Instagram in a few simple steps. First, choose your pack & target audience, then simply purchase! Let us grow your Instagram.</p><br><br>
                </div>
            </div>
            <div class="contentsDiv">
            <form id="registerForm" method="POST">
                {{ csrf_field() }}
                <div class="field-list clear">
                    <div class="col-lg-12" style="font-size:22px;font-weight:300;"><br>Your First Name <span class="required">*</span></div>
                    <div class="col-lg-12">
                        <input class="form-control" name="firstname" type="text" style="font-size:22px;font-weight:300;height:50px;" required>
                    </div>
                    <div class="col-lg-12" style="font-size:22px;font-weight:300;"><br>Your Last Name <span class="required">*</span></div>
                    <div class="col-lg-12">
                        <input class="form-control" name="lastname" type="text" style="font-size:22px;font-weight:300;height:50px;" required>
                    </div>
                    <div class="col-lg-12" style="font-size:22px;font-weight:300;"><br>Instagram Username <span class="required">*</span></div>
                    <div class="col-lg-12">
                        <input class="form-control" name="instagramusername" type="text" style="font-size:22px;font-weight:300;height:50px;" required>
                    </div>
                    <div class="col-lg-12" style="font-size:22px;font-weight:300;"><br>Instagram Password <span class="required">*</span></div>
                    <div class="col-lg-12">
                        <input class="form-control" name="instagrampassword" type="password" style="font-size:22px;font-weight:300;height:50px;" required>
                    </div>
                    <div class="col-lg-12" style="font-size:22px;font-weight:300;"><br>Email Address <span class="required">*</span></div>
                    <div class="col-lg-12">
                        <input class="form-control" name="email" type="email" style="font-size:22px;font-weight:300;height:50px;" required>
                    </div>

                    <div class="col-lg-12" style="font-size:22px;font-weight:300;<?php
                            if($_GET['plan'] == 'starter-kit-1000-followers' || $_GET['plan'] == 'rising-star-5000-followers' || $_GET['plan'] == 'influencer-10K-followers'){
                                echo 'display:none;';
                            }else{
                                echo '';
                            }
                        ?>"><br>Enter The #Hashtags You Want to Target or any other relevant information around your target audience including usernames of competitors who's followers you'd like to target or we can even target the followers of accounts with a similar concept to your own. <span class="required">*</span></div>
                    <div class="col-lg-12">
                        <textarea class="form-control" name="hashtags" rows="4"
                        <?php
                            if($_GET['plan'] == 'starter-kit-1000-followers' || $_GET['plan'] == 'rising-star-5000-followers' || $_GET['plan'] == 'influencer-10K-followers'){
                                echo ' style="display:none;"';
                            }else{
                                echo ' required';
                            }
                        ?>></textarea><br>
                    </div>

                    <div class="col-lg-12" style="font-size:22px;font-weight:300;<?php
                            if($_GET['plan'] == 'starter-kit-1000-followers' || $_GET['plan'] == 'rising-star-5000-followers' || $_GET['plan'] == 'influencer-10K-followers'){
                                echo 'display:none;';
                            }else{
                                echo '';
                            }
                        ?>"><br>(Campaign) If you want to automate commenting, enter at least 5 broad phrases that you would like your account to comment on other people content to strengthen engagement. <span class="required">*</span></div>
                    <div class="col-lg-12">
                        <textarea class="form-control" name="campaign" rows="4"
                        <?php
                            if($_GET['plan'] == 'starter-kit-1000-followers' || $_GET['plan'] == 'rising-star-5000-followers' || $_GET['plan'] == 'influencer-10K-followers'){
                                echo ' style="display:none;"';
                            }else{
                                echo ' required';
                            }
                        ?>></textarea><br>
                    </div>

                </div>
                <input id="plan" name="plan" type="text" value="<?php if(isset($_GET['plan'])){ echo $_GET['plan']; }?>
                " hidden>
                <input id="pass" name="password" type="password" hidden>
                <br style="clear:both;">
                <div class="btnblack" class="col-lg-12" style="text-align:center;">
                    <br stle="clear:both;">
                    <a class="btnb" id="register" href="#" data-cb-type="checkout" data-cb-plan-id="<?php if(isset($_GET['plan'])){ echo $_GET['plan'];}?>
                " > GET STARTED </a>
                    <input class="btnb" type="button" value="GET STARTED" hidden>
                </div>
                <br style="clear:both;"><br>
                <p class="fptmh3" style="text-align:center;width:100%;max-width:461px;margin:0 auto;font-size:32px;">MONEY BACK GUARANTEE</p><br>
                <p class="fptmhp" style="text-align:center;width:100%;max-width:461px;margin:0 auto;">If your account doesn't see growth we will give you a refund!</p><br>
                <input type="hidden" name="_token" value="{{ Session::token() }}">

            </form>

            <hr style="clear:both;">
            <br><br><br><br><br><br>

            </div>
<script src="https://js.chargebee.com/v2/chargebee.js" data-cb-site="socialite-test"> </script>
<script>
    $(document).ready(function(){
        var randomstring = Math.random().toString(36).slice(-8);
        $('#pass').val(randomstring);

        $('#register').on('click',function(e){

            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var frm=$('#registerForm');
            $.ajax({
                url: '/startnowRegister',
                type: 'POST',
                data: frm.serialize(),
                success: function(e){
                    console.log(e);
                }
            });
        });
    });

</script>

@endsection
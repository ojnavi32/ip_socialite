@extends('layouts.app')
@section('content')
    <style>
        .card-body{
            font-family:"futura-pt";
            font-size:20px;
            font-weight:300;
        }
        .card-header{
            height:75px;
            position:relative;
        }
        .card-title{
            position:absolute;
            font-size:22px;
            height:80px;
            top:20px;
        }
        .boxed ul{
            width:100%;
            max-width:220px;
            margin: 0 auto;
        }
        .boxed {
            height:650px;
        }
        .btnblack a:hover{
            color:gray;
            text-decoration:none!important;
        }
    </style>
        <div class="content">
            <div class="contentsDiv">
                <div class="tbtext" style="min-height:600px;">
                    <br><br><br><br><br><br>
                    <p class="fpt70" style="max-width:850px;">Buy Instagram Likes for Video & Photo Posts</p>

                </div>
                <div class="topimage" style="height:593px;">
                    <div class="color-overlay" style="background:rgb(33,27,23);;position:relative;opacity:.45"></div>
                    <div class="imageback" style="background-image:url('/images/image3.jpg');background-position: center;opacity:0.45;"></div>
                </div>
            </div>
            <div class="contentsDiv">
                <div class="twtextlp">
                    <h3 style="font-size:44px;text-align:center;font-family:'futura-pt';font-weight:300">Choose From Any of the Following Plans for Your Budget & Goals.</h3>
                    <br><br>
                    <p style="text-align:center;font-size:21px;font-family:'europa';font-weight:300">With subscription packages you sign up once and we take care of everything else. Instantly receive a set number of likes to every Instagram post that you make. There are no commitments so you can cancel your subscription any time. Subscription likes will cover 2 posts per day or 60 posts per month. Our likes come from random, premium accounts only. We will never put your account at risk, guaranteed!<br><br>
                    If you need a custom campaign put together please don’t hesitate to reach out to us.</p><br><br>
                    <hr style="clear:both;">
                    <br><br>
                </div>
            </div>
            <div class="contentsDiv">
                <div class="twtextlp">
                    <div class="row no-pad">
                        <div class="coltab3b">
                            <div class="boxed">
                                <h5>Instagram Likes</h5>
                                <h3>No Strings</h3>
                                <h5>Package</h5>
                                <h2><span>$ </span>30<span>/month</span></h2>
                                <br>
                                <div class="brkline"></div>
                                <ul class="col">
                                    <li>100 Likes Per Post</li>
                                    <li>60 Posts Per Month</li>
                                    <li>Auto Delivery</li>
                                    <li>You Can Cancel Anytime</li>
                                </ul><br><br>
                                <div class="startrs" ><a href="/startnow?plan=instagram-likes-no-strings">Start Today</a></div>
                            </div>
                        </div>
                        <div class="coltab3b">
                            <div class="boxed">
                                <h5>Instagram Likes</h5>
                                <h3>Bread & Butter</h3>
                                <h5>Package</h5>
                                <h2><span>$ </span>75<span>/month</span></h2>
                                <br>
                                <div class="brkline"></div>
                                <ul class="col">
                                    <li>500 Likes Per Post</li>
                                    <li>60 Posts Per Month</li>
                                    <li>Auto Delivery</li>
                                    <li>100% Secure</li>
                                    <li>You Can Cancel Anytime</li>
                                </ul><br><br>
                                <div class="startrs" ><a href="/startnow?plan=instagram-likes-bread-and-butter">Start Today</a></div>
                            </div>
                        </div>
                        <div class="coltab3b">
                            <div class="boxed">
                                <h5>Instagram Likes</h5>
                                <h3>Committed</h3>
                                <h5>Package</h5>
                                <h2><span>$ </span>125<span>/month</span></h2>
                                <br>
                                <div class="brkline"></div>
                                <ul class="col">
                                    <li>1000 Likes Per Post</li>
                                    <li>60 Posts Per Month</li>
                                    <li>Auto Delivery</li>
                                    <li>100% Secure</li>
                                    <li>You Can Cancel Anytime</li>
                                </ul><br><br>
                                <div class="startrs" ><a href="/startnow?plan=instagram-likes-committed">Start Today</a></div>
                            </div>
                        </div>
                    </div>
                    
                    <br style="clear:both;"><br><br>
                    <hr style="clear:both;">
                    <br><br>
                    <div class="contentsDiv">
                        <h3 style="font-size:44px;text-align:center;font-family:'futura-pt';font-weight:300">FAQ</h3>
                    <br><br>
                <div class="sectionb" style="width:100%;max-width:600px;margin:0 auto;">

                    <div class="container" style="font-family:'futura-pt';font-size:28px;font-weight:500">
                        <div class="accordion indicator-plus-before round-indicator" id="accordionH" aria-multiselectable="true">
                            <div class="card m-b-0">
                                <div class="card-header collapsed" role="tab" id="headingOneH" href="#collapseOneH" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapseOneH">
                                    <a class="card-title">Why should I buy Instagram likes?</a>
                                </div>
                                <div class="collapse" id="collapseOneH" role="tabpanel" aria-labelledby="headingOneH">
                                    <div class="card-body">
                                    It’s a great way to get an Instagram accounts noticed. Not everyone has a huge advertising budget or a legion of followers so buying likes is the best way to get your account seen among the million of other accounts. According to statistics, an individual is 90% more likely to like your posts and follower your account if they see that others are doing the same.  
                                    </div>
                                </div>
                                <div class="card-header collapsed" role="tab" id="headingTwoH" href="#collapseTwoH" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapseTwoH">
                                    <a class="card-title">How do I choose a package?</a>
                                </div>
                                <div class="collapse" id="collapseTwoH" role="tabpanel" aria-labelledby="headingTwoH">
                                    <div class="card-body">
                                    Choosing a package is a relatively easy process. Simply decide how little or how many likes you want to receive to each post. We have clients that receive up to 10k likes per post or as little as 30 likes post. It’s really up to you. Keep in mind that it will never be exactly that amount to look more natural. You can easily upgrade or downgrade your package at any time in the future. 
                                    </div>
                                </div>
                                
                                <div class="card-header collapsed" role="tab" id="headingThreeH" href="#collapseThreeH" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapseThreeH">
                                    <a class="card-title">Can I get a customized package?</a>
                                </div>
                                <div class="collapse" id="collapseThreeH" role="tabpanel" aria-labelledby="headingThreeH">
                                    <div class="card-body">
                                    Yes! Just contact us at hello@socialite.online. We can create a custom package. 

                                    </div>
                                </div>

                                <div class="card-header collapsed" role="tab" id="heading01H" href="#collapse01H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse01H">
                                    <a class="card-title">How do I cancel a subscription?</a>
                                </div>
                                <div class="collapse" id="collapse01H" role="tabpanel" aria-labelledby="heading01H">
                                    <div class="card-body">
                                    We have support specialists available who can answer any questions or concerns that you may have. Your feedback is important to us.
                                    </div>
                                </div>
                                
                            </div>
                        </div>	
                    </div>


                </div>
            </div>
            <br stle="clear:both;"><br>
            <div class="contentsDiv">
                <div class="twtextlp" style="width:100%;max-width:600px;margin:0 auto;">
                    <h3 style="font-size:44px;text-align:center;font-family:'futura-pt';font-weight:300">Have Questions?</h3>
                    <br><br>
                    <p style="text-align:center;font-size:21px;font-family:'europa';font-weight:300">We have support specialists available who can answer any questions or concerns that you may have. Your feedback is important to us.</p><br>
                </div>
                <div class="btnblack" style="text-align:center;"><a class="btnb" href="/contact" style="font-size:16px;font-family:futura-pt;font-weight:300">CONTACT SUPPORT</a></div><br><br>
                <br><br>
            </div>
            
        </div>
    </div>

@endsection
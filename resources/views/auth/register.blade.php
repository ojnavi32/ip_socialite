@extends('layouts.app')
@section('content')
        <div class="content" style="font-family:futura-pt;">
            <div class="contentsDiv">
                <div class="tbtext" style="min-height:520px;">
                    <br><br><br><br><br><br>
                    <p class="fpt70">Start Now</p>

                </div>
                <div class="topimage" style="height:516px;">
                    <div class="color-overlay" style="background:rgb(33,27,23);;position:relative;opacity:.45"></div>
                    <div class="imageback" style="background-image:url('/images/original_558373975.jpg');background-position: center;opacity:0.45;"></div>
                </div>
            </div>
            <div class="contentsDiv">
                <div class="twtextlp">
                    <h3 style="font-size:42px;text-align:center;">GET STARTED!</h3>
                    <br><br>
                    <p style="text-align:center;font-size:22px;font-weight:300;">Easily start growing your Instagram in a few simple steps. First, choose your pack & target audience, then simply purchase! Let us grow your Instagram.</p><br><br>
                </div>
            </div>
            <div class="contentsDiv">
            <form autocomplete="on" action="/startaccount" method="POST">
                {{ csrf_field() }}
                <div class="field-list clear">
                    <div class="col-lg-12" style="font-size:22px;font-weight:300;"><br>Your Full Name <span class="required">*</span></div>
                    <div class="col-lg-12">
                        <input class="form-control" name="name" type="text" style="font-size:22px;font-weight:300;height:50px;">
                    </div>
                    <div class="col-lg-12" style="font-size:22px;font-weight:300;"><br>Instagram Name <span class="required">*</span></div>
                    <div class="col-lg-12">
                        <input class="form-control" name="instagramname" type="text" style="font-size:22px;font-weight:300;height:50px;">
                    </div>
                    <div class="col-lg-12" style="font-size:22px;font-weight:300;"><br>Email Address <span class="required">*</span></div>
                    <div class="col-lg-12">
                        <input class="form-control" name="email" type="email" style="font-size:22px;font-weight:300;height:50px;">
                    </div>

                    <div class="col-lg-12" style="font-size:22px;font-weight:300;"><br>Enter The #Hashtags You Want to Target or any other relevant information around your target audience (You can even enter usernames of competitors who's followers you'd like to target or we can even target the followers of accounts with a similar concept to your own). <span class="required">*</span></div>
                    <div class="col-lg-12">
                        <textarea class="form-control" name="hashtags" rows="4"></textarea>
                    </div>
                </div>
                <input id="plan" name="plan" type="text" value="<?php echo $_GET['plan'];?>" >
                <input id="pass" name="password" type="password" >
                <br style="clear:both;">
                <div class="col-lg-12" style="text-align:center;">
                    <br stle="clear:both;">
                    <input class="btnb" type="submit" value="GET STARTED">
                </div>
                <br style="clear:both;"><br>
                <p class="fptmh3" style="text-align:center;width:100%;max-width:461px;margin:0 auto;font-size:32px;">MONEY BACK GUARANTEE</p><br>
                <p class="fptmhp" style="text-align:center;width:100%;max-width:461px;margin:0 auto;">If your account doesn't see growth we will give you a refund!</p><br>

            </form>
            <hr style="clear:both;">
            <br><br><br><br><br><br>

            </div>
<script>
    $(document).ready(function(){
        var randomstring = Math.random().toString(36).slice(-8);
        $('#pass').val(randomstring);
    });
</script>

@endsection

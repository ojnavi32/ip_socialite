@extends('layouts.app')
@section('content')
<div class="content">
    <div class="contentsDiv">
        <div class="tbtext" style="min-height:121px;">
        </div>
        <div class="topimage" style="height:121px;">
            <div class="color-overlay" style="background:#151515;position:relative;"></div>
        </div>
    </div>
    <div class="contentsDiv" style="font-family:'futura-pt';margin-bottom:50px;">
        <!-- Termly Tracking Code -->

        <iframe width="100%" style="height:105vh;overflow-y:hidden;" src="https://app.termly.io/document/terms-of-use-for-ecommerce/ced87a75-4800-4912-a1bb-542fddf877e8" frameborder="0" allowfullscreen>
        <p>Your browser does not support iframes.</p>
        </iframe>
    </div>

</div>

@endsection
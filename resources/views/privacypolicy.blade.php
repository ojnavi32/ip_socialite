@extends('layouts.app')
@section('content')
<div class="content">
    <div class="contentsDiv">
        <div class="tbtext" style="min-height:121px;">
        </div>
        <div class="topimage" style="height:121px;">
            <div class="color-overlay" style="background:#151515;position:relative;"></div>
        </div>
    </div>
    <div class="contentsDiv" style="font-family:'futura-pt';margin-bottom:50px;">
        <!-- Termly Tracking Code -->

        <iframe width="100%" style="height:105vh;overflow-y:hidden;" src="https://app.termly.io/document/privacy-policy-for-website/a9159f56-5835-4d2f-9029-4e5c34415d2f" frameborder="0" allowfullscreen>
        <p>Your browser does not support iframes.</p>
        </iframe>
    </div>

</div>

@endsection
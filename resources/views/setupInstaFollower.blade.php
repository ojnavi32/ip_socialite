@extends('layouts.app')
@section('content')
<style>
    .form-group label{
        font-weight:300;
        font-size:18px;
    }
</style>
<?php
        $nameArr = explode(' ',Auth::user()->name);
        $fname = $nameArr[0];
        $lname = end($nameArr);
        $post_data = array(
            'email'=>Auth::user()->email, 
            'password'=>Auth::user()->reference, 
            'f_name'=>Auth::user()->name,
            'first_name'=>$fname,
            'last_name'=>$lname,
            'instagram_name'=>Auth::user()->instagramname, 
            'hashtags'=>Auth::user()->hashtags, 
            'subscription_plan'=>Auth::user()->plan, 
            'status'=>'0'
        );
            
        pushMauticForm($post_data, 1, $_SERVER["REMOTE_ADDR"]);
        
        
        function pushMauticForm($data, $formId, $ip = null){
            // Get IP from $_SERVER
            if (!$ip) {
                $ipHolders = array(
                    'HTTP_CLIENT_IP',
                    'HTTP_X_FORWARDED_FOR',
                    'HTTP_X_FORWARDED',
                    'HTTP_X_CLUSTER_CLIENT_IP',
                    'HTTP_FORWARDED_FOR',
                    'HTTP_FORWARDED',
                    'REMOTE_ADDR'
                );
                foreach ($ipHolders as $key) {
                    if (!empty($_SERVER[$key])) {
                        $ip = $_SERVER[$key];
                        if (strpos($ip, ',') !== false) {
                            // Multiple IPs are present so use the last IP which should be the most reliable IP that last connected to the proxy
                            $ips = explode(',', $ip);
                            array_walk($ips, create_function('&$val', '$val = trim($val);'));
                            $ip = end($ips);
                        }
                        $ip = trim($ip);
                        break;
                    }
                }
            }
            
            $data['formId'] = $formId;
            // return has to be part of the form data array
            if (!isset($data['return'])) {
                $data['return'] = 'http://socialitedev.kohlkreative.com/myaccount';
            }
        
            $data = array('mauticform' => $data);
            // Change [path-to-mautic] to URL where your Mautic is
            $formUrl =  'http://socialitedev.kohlkreative.com/mautic/form/submit?formId=' . $formId;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $formUrl);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Forwarded-For: $ip"));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
        
        }

?>
<div class="content">
    <div class="contentsDiv">
        <div class="tbtext" style="min-height:121px;">
        </div>
        <div class="topimage" style="height:121px;">
            <div class="color-overlay" style="background:#151515;position:relative;"></div>
        </div>
    </div>
    <div class="contentsDiv" style="font-family:'futura-pt';font-weight:300;">
        <h2 style="text-align:center;">Complete Your Account Setup</h2><br><br>
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif

        <section>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <form action="/updateaccount/{{Auth::user()->id}}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group"><label>Instagram Handle (Name) *</label><input class="form-control" type="text" name="instagramname" value="{{Auth::user()->instagramname}}"></div>
                            <div class="form-group"><label>Phone # *</label><input class="form-control" type="text" name="phone"></div>
                            <div class="form-group"><label>Email Address *</label><input class="form-control" type="email" name="email" value="{{Auth::user()->email}}" readonly></div>
                            <div class="form-group" style="display:none!important"><label style="width:100%;">Instagram Password *</label><label>Often times people will just change their password to something simple. "Your information is protected with SSL protection"</label><input class="form-control" type="password" name="instampassword"></div>
                            <div class="form-group" style="display:none!important"><label style="width:100%;">Confirm Your Instagram Password *</label><input class="form-control" type="password" name="instampasswordconfirm"></div>
                    <div class="form-group" style="display:none!important"><label style="width:100%;">4-5 generic that we can use in your campaign**</label><textarea class="form-control" rows="5" name="campaign"></textarea></div>
                    <div class="form-group" style="text-align:center;margin:40px 0px;"><button class="btnb" type="submit">COMPLETE SETUP</button></div>
                    <div class="form-group" style="display:none!important">
                        <p>**These should be comments that will work with almost any Instagram picture. A lot of people will use Emoji's or comments like "That's awesome", "Keep up the good work", "I love your content" etc. You probably get the point now so just put your own spin on it. Also if you need any help setting up your campaign you can email or call us.</p>
                    </div>
                    </form>
                </div>
                <div class="col" style="background-image:url(&quot;/images/image22.jpg&quot;);background-size:cover;background-repeat:no-repeat;background-position:bottom;"></div>
            </div>
            </div>
        </section>
        <br><br> 
    </div>
</div>

@endsection
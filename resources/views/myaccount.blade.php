@extends('layouts.app')
@section('content')
        <style>
            .active{
                font-style:'futura-pt'!important;
                font-weight:500!important;
                color:gray!important;
            }
            #myaccount li a{
                font-style:'futura-pt'!important;
                font-weight:300!important;
                color:black!important;
            }
            .form-group a{
                text-decoration:none!important;
            }
            .form-group a:hover{
                color:gray;
            }
        </style>
        <div class="content">
            <div class="contentsDiv">
                <div class="tbtext" style="min-height:121px;">
                </div>
                <div class="topimage" style="height:121px;">
                    <div class="color-overlay" style="background:#151515;position:relative;"></div>
                </div>
            </div>
            <section>
                <div class="container">
                    <br>
                    <div class="row">
                        <div class="col-12">
                            <h2>My Account Details</h2>
                            <br>
                            <div>
                                <ul class="nav nav-tabs" id="myaccount">
                                    <li class="nav-item" hidden><a class="nav-link active" role="tab" data-toggle="tab" href="#tab-1">Subscriptions</a></li>
                                    <li class="nav-item" hidden><a class="nav-link" role="tab" data-toggle="tab" href="#tab-2">Other Services</a></li>
                                    <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#tab-3"></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" role="tabpanel" id="tab-1" hidden>
                                        <br>
                                        <div class="row">
                                            <div class="col">
                                                <div class="table-responsive-md">
                                                    <table class="table table-striped table-bordered table-hover table-sm">
                                                        <thead>
                                                            <tr>
                                                                <th>Subscription</th>
                                                                <th>Date Started</th>
                                                                <th>Expiry Date</th>
                                                                <th>Status</th>
                                                                <th>Price</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="tab-2" hidden>
                                        <br>
                                        <div class="row">
                                            <div class="col">
                                                <h5>Instragram Likes</h5>
                                                <div class="table-responsive-md">
                                                    <table class="table table-striped table-bordered table-hover table-sm">
                                                        <thead>
                                                            <tr>
                                                                <th>Product</th>
                                                                <th>Date Purchased</th>
                                                                <th>Delivery Date</th>
                                                                <th>Status</th>
                                                                <th>Price</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <h5>Instragram Followers</h5>
                                                <div class="table-responsive-md">
                                                    <table class="table table-striped table-bordered table-hover table-sm">
                                                        <thead>
                                                            <tr>
                                                                <th>Product</th>
                                                                <th>Date Purchased</th>
                                                                <th>Delivery Date</th>
                                                                <th>Status</th>
                                                                <th>Price</th>
                                                                <th>Actions</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 1</td>
                                                                <td>Cell 2</td>
                                                                <td>Cell 2</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane active" role="tabpanel" id="tab-3">
                                        <div class="row" id="displayProfile">
                                            
                                            <div class="col-12"><br><br>
                                                <h5>Profile Information</h5><br>
                                            </div>
                                            <div class="col">
                                                <div class="row">
                                                    <div class="col-4">
                                                        <p>Fullname</p>
                                                    </div>
                                                    <div class="col">
                                                        <p class="lead">{{Auth::user()->name}}<br></p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <p>Email</p>
                                                    </div>
                                                    <div class="col">
                                                        <p class="lead">{{Auth::user()->email}}<br></p>
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <p>Instagram Name</p>
                                                    </div>
                                                    <div class="col">
                                                        <p class="lead">{{Auth::user()->instagramname}}<br></p>
                                                        
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <p>Phone</p>
                                                    </div>
                                                    <div class="col">
                                                        <p class="lead">{{Auth::user()->phone}}<br></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <p>Hashtags</p>
                                                    </div>
                                                    <div class="col-12">
                                                        <p class="lead">{{Auth::user()->hashtags}}<br></p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <p>Campaign</p>
                                                    </div>
                                                    <div class="col-12">
                                                        <p class="lead">{{Auth::user()->campaign}}<br></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="updateProfile" style="display:none;">
                                            <div class="col-12"><br><br>
                                                <h5>Update Profile Information</h5><br>
                                            </div>
                                            <div class="col">
                                                <form id="updateFForm" action="/updateProfile" method="POST">
                                                    {{ csrf_field() }}
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>Full Name</label><input class="form-control" type="text" name="name" value="{{Auth::user()->name}}"></div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group"><label>Email</label><input class="form-control" type="text" name="email" value="{{Auth::user()->email}}"></div>
                                                        </div>
                                                        <div class="col" style="margin-left:40px;">
                                                            <div class="form-group" style="line-height:2.2em;">
                                                                <label>Account Password</label>
                                                                <br>
                                                                <a class="btnb" href="/changePasswordPage" target="_blank">CHANGE</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>Instagram</label><input class="form-control" type="text" name="instagramname" value="{{Auth::user()->instagramname}}"></div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group"><label>Phone</label><input class="form-control" type="text" name="phone" value="{{Auth::user()->phone}}"></div>
                                                        </div>
                                                        <div class="col" style="margin-left:40px;">
                                                            <div class="form-group" style="line-height:2.2em;">
                                                                <label>Instagram Password</label>
                                                                <br>
                                                                <a class="btnb" href="/changeInstagramPasswordPage" target="_blank">CHANGE</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col">
                                                            <div class="form-group"><label>Hashtags</label><textarea class="form-control" name="hashtags" rows="6">{{Auth::user()->hashtags}}</textarea></div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group"><label>Campaign</label><textarea class="form-control" name="campaign" rows="6">{{Auth::user()->campaign}}</textarea></div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br><br><br>
                            <div class="form-group" style="text-align:center;margin:40px 30px;">
                            <button id="cancelUpdate" class="btnb" type="button" style="display:none;margin-right:40px;">CANCEL</button>
                            <button id="updateShow" class="btnb" type="button">EDIT PROFILE</button></div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
<script>
    $(document).ready(function(){
        $('#updateShow, #cancelUpdate').on('click',function(e){
            e.preventDefault();
            var txt = $('#updateShow').text();
            if(txt==='EDIT PROFILE'){
                console.log(txt);
                $('#updateShow').text('SAVE PROFILE');
                $('#displayProfile').hide();
                $('#updateProfile').show();
                $('#cancelUpdate').show();
            }else{
                console.log(txt + 'submit form');
                var clck=$(this).text();
                if(clck==='SAVE PROFILE'){
                    $('#updateFForm', document).submit();
                }
                $('#updateShow').text('EDIT PROFILE');
                $('#displayProfile').show();
                $('#updateProfile').hide();
                $('#cancelUpdate').hide();
            }
        })
    })
</script>
@endsection
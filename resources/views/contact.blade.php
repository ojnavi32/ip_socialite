@extends('layouts.app')
@section('content')
        <style>
            .iconimg{
                width:90%;
                height:80px;
                margin:0px auto;
            }

            .card-body{
                font-family:"futura-pt";
                font-size:20px;
                font-weight:300;
            }
            .card-header{
                height:75px;
                position:relative;
            }
            .card-title{
                position:absolute;
                font-size:22px;
                height:80px;
                top:20px;
            }

        </style>
        <div class="content">
            <div class="contentsDiv">
                <div class="tbtext" style="min-height:600px;">
                    <br><br><br><br><br><br>
                    <p class="fpt70">SUPPORT & CONTACT</p>

                    <p class="fpt36">IF YOU HAVE ANY IDEAS, QUESTIONS OR FEEDBACK</p>
                    <br>
                </div>
                <div class="topimage" style="height:572px;">
                    <div class="color-overlay" style="background:rgb(33,27,23);;position:relative;opacity:.45"></div>
                    <div class="imageback" style="background-image:url('/images/image2.jpg');background-position: center;opacity:0.45;"></div>
                </div>
            </div>
            <div class="contentsDiv" style="font-family:futura-pt;">
                <div class="twtextlp">
                    <h3 style="font-size:32px;text-align:center;font-weight:300">WHAT CAN WE HELP YOU WITH?</h3>
                    <br><br>
                    <p style="text-align:center;font-size:22px;font-weight:300">Find your answer in our help center below!</p><br><br>
                    <h3 style="font-size:48px;text-align:center;font-weight:700">Additional FAQ</h3>
                    <br><br>
                </div>
            </div>

            <div class="contentsDiv">
                <div class="sectionb">

        <div class="container" style="font-family:'futura-pt';font-size:28px;font-weight:500">
			<div class="accordion indicator-plus-before round-indicator" id="accordionH" aria-multiselectable="true">
				<div class="card m-b-0">
					<div class="card-header collapsed" role="tab" id="headingOneH" href="#collapseOneH" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapseOneH">
						<a class="card-title">What happens after I’ve paid?</a>
					</div>
					<div class="collapse" id="collapseOneH" role="tabpanel" aria-labelledby="headingOneH">
						<div class="card-body">
                        Your account manager will contact you directly to put to discuss your marketing campaign and put together a plan to achieve the results you’re looking for. It takes 12-24 hours to setup an account after activation. 
						</div>
					</div>
					<div class="card-header collapsed" role="tab" id="headingTwoH" href="#collapseTwoH" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapseTwoH">
						<a class="card-title">How quickly will I start to see results?</a>
					</div>
					<div class="collapse" id="collapseTwoH" role="tabpanel" aria-labelledby="headingTwoH">
						<div class="card-body">
                        While results will very from account to account, you will start seeing action on your profile relatively quickly. There are several factors to consider when checking your results such as your target audience, your starting number of followers/average likes etc. However at Socialite we make it easy for you by sending you a weekly report tracking your stats. 
						</div>
                    </div>
                    
					<div class="card-header collapsed" role="tab" id="headingThreeH" href="#collapseThreeH" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapseThreeH">
						<a class="card-title">Does Socialite do gender targeting?</a>
					</div>
					<div class="collapse" id="collapseThreeH" role="tabpanel" aria-labelledby="headingThreeH">
						<div class="card-body">
                        Gender targeting is not public data and so none of our targeting is by gender. 

						</div>
                    </div>

					<div class="card-header collapsed" role="tab" id="heading01H" href="#collapse01H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse01H">
						<a class="card-title">What if I want to change my campaign?</a>
					</div>
					<div class="collapse" id="collapse01H" role="tabpanel" aria-labelledby="heading01H">
						<div class="card-body">
                        Your account manager will be in touch with you on a weekly basis. Should you need any changes such as upgrading your account or changing your targeting they can absolutely help with that. Additionally your account manager will be available at your disposal to help at any time. Simply reach out to them.
						</div>
                    </div>
                    

					<div class="card-header collapsed" role="tab" id="heading02H" href="#collapse02H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse02H">
						<a class="card-title">I want to upgrade my plan?</a>
					</div>
					<div class="collapse" id="collapse02H" role="tabpanel" aria-labelledby="heading02H">
						<div class="card-body">
                        No problem, we can absolutely do that for you. However there is a 72 hour change window for subscription changes but we will pro-rate your account with that amount towards the new plan. 
						</div>
                    </div>

					<div class="card-header collapsed" role="tab" id="heading03H" href="#collapse03H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse03H">
						<a class="card-title">Can I still use Instagram as normal while working with Socialite?</a>
					</div>
					<div class="collapse" id="collapse03H" role="tabpanel" aria-labelledby="heading03H">
						<div class="card-body">
                        Absolutely, you can continue to post, like, follow and anything else you would normally do. You can also reach out to your account manager for some tips and tricks that you can do with your posts to help synergistically increase your results. 
						</div>
                    </div>

					<div class="card-header collapsed" role="tab" id="heading04H" href="#collapse04H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse04H">
						<a class="card-title">Are these real followers?</a>
					</div>
					<div class="collapse" id="collapse04H" role="tabpanel" aria-labelledby="heading04H">
						<div class="card-body">
                        Yes, these are 100% real followers. Socialite was birthed out of a quest for the best Instagram growth techniques. Fake followers can be good for a quick boost which is why we still offer that under our other services section but all of our organic growth packages will only provide real followers.
						</div>
                    </div>


					<div class="card-header collapsed" role="tab" id="heading05H" href="#collapse05H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse05H">
						<a class="card-title">Can you do a customized campaign for me?</a>
					</div>
					<div class="collapse" id="collapse05H" role="tabpanel" aria-labelledby="heading05H">
						<div class="card-body">
                        All of our campaigns are customized to the individuals needs and goals however if you need us to do something very specific you can reach out to our customer support team and we will do our very best to come up with a solution to your needs.
						</div>
                    </div>

					<div class="card-header collapsed" role="tab" id="heading06H" href="#collapse06H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse06H">
						<a class="card-title">What will happen to my results if I cancel service?</a>
					</div>
					<div class="collapse" id="collapse06H" role="tabpanel" aria-labelledby="heading06H">
						<div class="card-body">
                        Nothing will happen to your results. These are real followers and likes that are interacting with you because of the marketing efforts we’ve taken on your behalf and because of your content. They will not go anywhere, however your future growth will likely slow down unless you can continue on your own.	</div>
                    </div>

					<div class="card-header collapsed" role="tab" id="heading07H" href="#collapse07H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse07H">
						<a class="card-title">Why Are My Followers Going Down?</a>
					</div>
					<div class="collapse" id="collapse07H" role="tabpanel" aria-labelledby="heading07H">
						<div class="card-body">
                        There are two potential reasons for this. First if you ever bought fake followers in the past (no judgement, a lot of people have done it) Instagram is constantly purging fake accounts. So you could be losing a large amount of followers every single day. Secondly due to other services out there that use the Follow/Unfollow method to grow accounts it's very possible that you may simply be the recipient of being un-followed. In these cases our goal is to grow your net-followers so that the new followers you get each day is more than any lost followers. In these instances especially if you're losing followers due to previously purchasing fake followers it's better to watch your incoming new follower notifications rather than your total number of followers as an indication of results.                         
						</div>
                    </div>


					<div class="card-header collapsed" role="tab" id="heading08H" href="#collapse08H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse08H">
						<a class="card-title">Do you do the follow/unfollow method?</a>
					</div>
					<div class="collapse" id="collapse08H" role="tabpanel" aria-labelledby="heading08H">
						<div class="card-body">
                        No! This is one of the worst methods for building a following aside from purchasing fake followers. Nothing will annoy someone more than to see that you followed them just to unfollow in a few days. To build a true network on Instagram its imperative that you interact with your network and there is plenty of technology now that can track who is doing this.
						</div>
                    </div>


					<div class="card-header collapsed" role="tab" id="heading09H" href="#collapse09H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse09H">
						<a class="card-title">Can you still work with me if my account is private?</a>
					</div>
					<div class="collapse" id="collapse09H" role="tabpanel" aria-labelledby="heading09H">
						<div class="card-body">
                        Absolutely, although we do recommend keeping your account public there is no reason that your campaign will not work if privacy is a concern.
						</div>
                    </div>


					<div class="card-header collapsed" role="tab" id="heading10H" href="#collapse10H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse10H">
						<a class="card-title">Does Socialite provide analytics?</a>
					</div>
					<div class="collapse" id="collapse10H" role="tabpanel" aria-labelledby="heading10H">
						<div class="card-body">
                        Sure, we could provide you an anlaytics report within 2 business days of your account progress. 
						</div>
                    </div>


					<div class="card-header collapsed" role="tab" id="heading011H" href="#collapse011H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse011H">
						<a class="card-title">What if I want to change my username or account?</a>
					</div>
					<div class="collapse" id="collapse011H" role="tabpanel" aria-labelledby="heading011H">
						<div class="card-body">
                        This isn’t an issue just please keep your account manager informed so your campaign continues uninterrupted.                         
						</div>
                    </div>


					<div class="card-header collapsed" role="tab" id="heading012H" href="#collapse012H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse012H">
						<a class="card-title">How do I cancel my subscription?</a>
					</div>
					<div class="collapse" id="collapse012H" role="tabpanel" aria-labelledby="heading012H">
						<div class="card-body">
                        Just send an email to hello@socialite.online and your cancellation will be actioned immediately                        
						</div>
                    </div>


					<div class="card-header collapsed" role="tab" id="heading013H" href="#collapse013H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse013H">
						<a class="card-title">Can I change my method of payment?</a>
					</div>
					<div class="collapse" id="collapse013H" role="tabpanel" aria-labelledby="heading013H">
						<div class="card-body">
                        Yes, again just reach out to your account manager and they can handle this for you with no interruptions to your service.                        
						</div>
                    </div>

					<div class="card-header collapsed" role="tab" id="heading014H" href="#collapse014H" data-toggle="collapse" data-parent="#accordionH" aria-expanded="false" aria-controls="collapse014H">
						<a class="card-title">What if I don’t know who I want to target?</a>
					</div>
					<div class="collapse" id="collapse014H" role="tabpanel" aria-labelledby="heading014H">
						<div class="card-body">
                        Not an issue. Your account manager will work with you based on your goals to come-up with a suitable plan for your campaign.                        
						</div>
                    </div>





                </div>
			</div>	
</div>


                </div>
            </div>
            <br stle="clear:both;"><br>
            <div class="contentsDiv" style="font-family:futura-pt;">
                <div class="col-lg-8">
                    <h2 style="font-weight:500">Contact Us</h2><br>
                    <p style="font-weight:300;font-size:20px;">We would be glad to answer any questions you have, if you think a question needs to be added above, contact us!</p><br><br>

                    <form autocomplete="on" action="/pages/sendemail.php" method="POST">
                        <div class="field-list clear">
                            <div class="col-lg-12" style="font-size:22px;font-weight:300;">Name <span class="required">*</span></div>
                            <div class="col-lg-6">
                                <input class="form-control" name="fname" type="text" style="font-size:22px;font-weight:300;height:50px;">
                                <span style="font-size:16px;font-weight:300;line-spacing:2em;">First Name<br><br></span>
                            </div>
                            <div class="col-lg-6">
                                <input class="form-control" name="lname" type="text" style="font-size:22px;font-weight:300;height:50px;">
                                <span style="font-size:16px;font-weight:300;line-spacing:2em;">Last Name<br><br></span>
                            </div>
                            <div class="col-lg-12" style="font-size:22px;font-weight:300;">Email Address <span class="required">*</span></div>
                            <div class="col-lg-12">
                                <input class="form-control" name="email" type="email" style="font-size:22px;font-weight:300;height:50px;"><br>
                            </div>

                            <div class="col-lg-12" style="font-size:22px;font-weight:300;">Subject <span class="required">*</span></div>
                            <div class="col-lg-12">
                                <input class="form-control" name="subject" type="text" style="font-size:22px;font-weight:300;height:50px;"><br>
                            </div>
                            <div class="col-lg-12" style="font-size:22px;font-weight:300;">Message <span class="required">*</span></div>
                            <div class="col-lg-12">
                                <textarea class="form-control" rows="3" name="message" style="font-size:22px;font-weight:300;"></textarea>
                            </div>
                        </div>

                        <div class="col-lg-12" style="text-align:center;margin-bottom:75px;">
                            <br stle="clear:both;">
                           <input class="btnb" type="submit" value="SUBMIT">
                        </div>

                    </form>

                </div>
                <div class="col-lg-4" style="font-family:futura-pt;">
                    <p style="font-size:20px;font-weight:500">WORLDWIDE PHONE SUPPORT</p>
                    <p style="font-size:20px;font-weight:100">(800) 380-3924</p>
                    <br>
                    <p style="font-size:20px;font-weight:500">OUR EMAIL</p>
                    <p style="font-size:24px;font-weight:100">hello@socialite.online</p>
                </div>
                <br style="clear:both;">
            </div>
            <br stle="clear:both;"><br><br>
        </div>
        <script>
            $(document).ready(function(){

            $( '.closeall' ).click( function( e ) {
                e.preventDefault();
                    $( '.accordion .collapse.show' ).collapse( 'hide' );
                    return false;
                } );
                $( '.openall' ).click( function( y ) {
                    y.preventDefault();
                    $( '.accordion .collapse' ).collapse( 'show' );
                    return false;
                } );

                if ( window.location.hash ) {
                    redirect( window.location.hash );
                }

                $( 'a[href^="#"]' ).on( 'click', function( x ) {
                    x.preventDefault();
                    var a = document.createElement( 'a' );
                        a.href = this.href;
                    redirect ( a.hash );
                    return false;
                } );

                function redirect( hash ) {
                    // $( hash ).attr( 'aria-expanded', 'true' ).focus();
                    // $( hash + '+div.collapse' ).addClass( 'show' ).attr( 'aria-expanded', 'true' );
                    $( hash + '+div.collapse' ).collapse( 'show' );

                    // using this because of static nav bar space
                    $( 'html, body' ).animate( {
                        scrollTop: $( hash ).offset().top - 60
                    }, 10, function() {
                    // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    } );
                }

                document.documentElement.setAttribute("lang", "en");
                document.documentElement.removeAttribute("class");


            });

            </script>
@endsection